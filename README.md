# Lemon Drop

## Gitlab

### Local Runners

[Install Gitlab runners locally](https://docs.gitlab.com/runner/install/osx.html)

```
sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
cd ~
gitlab-runner install
gitlab-runner start
```

[Register the Gitlab runner](https://docs.gitlab.com/runner/register/#macos)
Make sure Docker.app is installed for Mac

```
sudo gitlab-runner register

# For the prompt enter:
# https://gitlab.com
# Token from Runners section of https://gitlab.com/KempfCreative/lemon-drop-bake-shop/-/settings/ci_cd 
# Hostname
# docker
# alpine:latest
```

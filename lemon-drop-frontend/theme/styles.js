import { createGlobalStyle } from 'styled-components';
import fonts from './fonts';
import reset from './reset';
import theme from './theme';

const GlobalStyles = createGlobalStyle`
  ${fonts}
  ${reset}
  html {
    box-sizing: border-box;
    font-size: 10px;
    background: 100px 50px / 100px repeat url("/images/lemon-slice.svg"),
    50px 20px / 100px repeat url("/images/lemon-slice.svg")
    ${theme.color.brandYellowBackground};
  }

  *, *::before, *::after {
    box-sizing: inherit;
  }

  body {
    -webkit-font-smoothing: antialiased;
    font-family: ${theme.font.body};
    font-size: 1.5rem;
    line-height: 1.5;
    margin: 0;
    padding: 0;
    text-rendering: optimizeLegibility;
  }

  .appMain {
    width: 90vw;
    border: 5px solid ${theme.color.neutral};
    border-radius: 2rem;
    background: ${theme.color.white};
    max-width: 1400px;
    min-height: 80vh;
    margin: 10vh auto 10vh auto;
  }

  .appLinks {
    width: 95%;
    margin: 0 auto;
    @media screen and (min-width: ${theme.breakpoint.small}) {
      margin: -2.5rem 2rem 2.5rem 2rem;
      display: flex;
      justify-content: space-between;
    }
  }

  .userWrapper {
    position: fixed;
    bottom: 0;
    display: flex;
    justify-content: space-between;
    width: 100vw;
    margin-left: -8.5vw;
    background: ${theme.color.white};
    padding-top: 1rem;
    z-index: ${theme.layers.audience};
    @media screen and (min-width: ${theme.breakpoint.small}) {
      position: static;
      bottom: unset;
      justify-content: unset;
      width: unset;
      margin-left: 0;
      background: transparent;
      padding-top: 0;
      z-index: unset;
    }
  }

  .pageNav {
    margin: 2rem auto;
    @media screen and (min-width: ${theme.breakpoint.small}) {
      margin: 0;
    }
  }

  .pageNav__listWrapper {
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    border-top: 2px solid ${theme.color.brandPurple};
    @media screen and (min-width: ${theme.breakpoint.small}) {
      border-top: 0;
      flex-direction: row;
    }
  }

  .pageNav__listWrapper-item {
    border-bottom: 2px solid ${theme.color.brandPurple};
    @media screen and (min-width: ${theme.breakpoint.small}) {
      border-bottom: 0;
    }
  }

  .pageNav__listWrapper-link {
    display: block;
    margin: 1rem 0;
    padding: 0.5rem 1rem;
    text-decoration: none;
    text-transform: uppercase;
    text-align: center;
    color: ${theme.color.brandPurpleDarker};
    font-weight: bold;
    font-size: ${theme.modularScale.base};
    &:hover {
      color: ${theme.color.brandPurpleDark};
      text-decoration: underline;
    }
    @media screen and (min-width: ${theme.breakpoint.small}) {
      text-align: left;
      padding: 1rem 3rem;
      font-size: ${theme.modularScale.medium};
    }
  }

  .accountLink {
    margin: 1rem 0;
    padding: 0.5rem 1rem;
    text-decoration: none;
    text-transform: uppercase;
    color: ${theme.color.brandPurpleDarker};
    font-weight: bold;
    font-size: ${theme.modularScale.base};
    &:hover {
      color: ${theme.color.brandPurpleDark};
      text-decoration: underline;
    }
    @media screen and (min-width: ${theme.breakpoint.small}) {
      padding: 1rem 3rem;
      font-size: ${theme.modularScale.medium};
    }
  }

  .pageHeader {
    margin: -5rem auto 0 auto;
    width: 15rem;
  }

  .pageHeader__logoWrapper {
    display: flex;
    cursor: pointer;
    transition: transform 0.5s;
    text-decoration: none;
    :hover {
      transform: rotate(5deg);
    }
  }

  .pageHeader__logoImage {
    height: 10rem;
    // TODO Get the elements to float next to the clip path or shape outside
    // shape-outside: url(images/lemon-drop-logo.png);
    // float: left;
  }

  .pageHeader__logoText {
    font-family: ${theme.font.head};
    font-size: ${theme.modularScale.medium};
    font-style: italic;
    font-weight: bold;
    color: ${theme.color.brandPurpleDark};
    width: 100%;
    word-spacing: 100%;
    padding: 0.8rem 0 0 1rem;
    line-height: 1.2;
  }
`;

export default GlobalStyles;

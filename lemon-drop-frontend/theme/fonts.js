import { css } from 'styled-components';

const fonts = css`
@font-face {
  font-family: "Bodoni 72";
  src: url('/fonts/Bodoni-72-Book.otf') format('opentype'),
  url('/fonts/Bodoni-72-Book.ttf') format('truetype');
  font-weight:  normal;
  font-style:   normal;
  font-stretch: normal;
}

@font-face {
  font-family: "Bodoni 72";
  src: url('/fonts/Bodoni-72-Book-Italic.otf') format('opentype'),
  url('/fonts/Bodoni-72-Book-Italic.ttf') format('truetype');
  font-weight: normal;
  font-style: italic;
  font-stretch: normal;
}

@font-face {
  font-family: "Bodoni 72";
  src: url('/fonts/Bodoni-72-Bold.otf') format('opentype'),
  url('/fonts/Bodoni-72-Bold.ttf') format('truetype');
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
}

@font-face {
  font-family: "Bodoni 72";
  src: url('/fonts/Bodoni-72-Bold-Italic.otf') format('opentype'),
  url('/fonts/Bodoni-72-Bold-Italic.ttf') format('truetype');
  font-weight: bold;
  font-style: italic;
  font-stretch: normal;
}

@font-face {
  font-family: "Bodoni 06";
  src: url('/fonts/Bodoni-06-Book.otf') format('opentype'),
  url('/fonts/Bodoni-06-Book.ttf') format('truetype');
  font-weight:  normal;
  font-style:   normal;
  font-stretch: normal;
}

@font-face {
  font-family: "Bodoni 06";
  src: url('/fonts/Bodoni-06-Book-Italic.otf') format('opentype'),
  url('/fonts/Bodoni-06-Book-Italic.ttf') format('truetype');
  font-weight: normal;
  font-style: italic;
  font-stretch: normal;
}

@font-face {
  font-family: "Bodoni 06";
  src: url('/fonts/Bodoni-06-Bold.otf') format('opentype'),
  url('/fonts/Bodoni-06-Bold.ttf') format('truetype');
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
}

@font-face {
  font-family: "Bodoni 06";
  src: url('/fonts/Bodoni-06-Bold-Italic.otf') format('opentype'),
  url('/fonts/Bodoni-06-Bold-Italic.ttf') format('truetype');
  font-weight: bold;
  font-style: italic;
  font-stretch: normal;
}
`;

export default fonts;

import { keyframes } from 'styled-components';

const animation = {
  fast: '200ms',
  default: '400ms',
  slow: '600ms',
  easeInQuad: 'cubic-bezier(0.550, 0.085, 0.68, 0.530)',
  easeOutQuad: 'cubic-bezier(0.390, 0.575, 0.565, 1.000)',
  easeInQuart: 'cubic-bezier(0.895, 0.030, 0.685, 0.220)',
  easeOutQuart: 'cubic-bezier(0.165, 0.840, 0.440, 1)',
  easeMove: 'cubic-bezier(0.15, 0.8, 0.3, 1.2)'
};

const animations = {
  wiggle: keyframes`
    0%  { transform: rotate(0) }
    25% { transform: rotate(6deg) }
    50% { transform: rotate(0deg) scale(1.025) }
    75% { transform: rotate(-6deg) }
    100% { transform: rotate(0) }
  `
};

const arrow = (direction, fill, position, size = 5) => {
  const x1 = size;
  const x2 = size * 2;

  const coords = {
    up: {
      path: `M0 ${x1} L${x2} ${x1} L${x1} 0 Z`,
      viewbox: `0 0 ${x2} ${x1}`
    },
    down: {
      path: `M0 0 L${x1} ${x1} L${x2} 0 Z`,
      viewbox: `0 0 ${x2} ${x1}`
    },
    left: {
      path: `M0 ${x1} L${x1} ${x2} L${x1} 0 Z`,
      viewbox: `0 0 ${x1} ${x2}`
    },
    right: {
      path: `M0 0 L0 ${x2} L${x1} ${x1} Z`,
      viewbox: `0 0 ${x1} ${x2}`
    }
  };

  const data = `
    <svg xmlns='http://www.w3.org/2000/svg' version='1.1' viewbox="${coords[direction].viewbox}">
      <path fill="${fill}" d="${coords[direction].path}" />
    </svg>
  `;

  return `
    background-image: url(data:image/svg+xml;base64,${Buffer.from(data).toString('base64')});
    background-position: ${position};
    background-repeat: no-repeat;
    background-size: ${(['up', 'down'].includes(direction)) ? `${x2}px ${x1}px` : `${x1}px ${x2}px`};
  `;
};

const breakpoint = {
  small: '768px',
  medium: '992px',
  large: '1200px',
  max: (bp) => `${parseInt(breakpoint[bp], 10) - 1}px`
};

const color = {
  white: '#FFFFFF',
  brandYellow: '#F4F4AC',
  brandYellowBackground: '#F3F2AE',
  brandYellowLight: '#FBFADC',
  brandPurple: '#CFABF5',
  brandPurpleDark: '#C6ABD2',
  brandPurpleDarker: '#8766A9',
  neutral: '#D7D6D6',
  mint: '#E3E9DD',
  error: '#EC1919'
};

const font = {
  head: '"Bodoni 72", Georgia, serif',
  body: '"Bodoni 06", Georgia, serif'
};

const layers = {
  backstage: 1,
  upstage: 1000,
  centerstage: 2000,
  downstage: 3000,
  audience: 4000
};

const mainBackground = `
  width: 90vw;
  border: 5px solid ${color.neutral};
  border-radius: 2rem;
  background: ${color.white};
  max-width: 1400px;
  min-height: 80vh;
  margin: 10vh auto 10vh auto;
`;

const modularScale = {
  small: '1.2rem',
  base: '1.5rem',
  medium: '1.875rem',
  large: '2.3438rem',
  xlarge: '2.9297rem',
  '2xlarge': '3.6621rem',
  '3xlarge': '4.5776rem',
  '4xlarge': '5.722rem'
};

const width = {
  small: '600px',
  medium: '900px',
  large: '1200px'
};

const GlobalTheme = {
  animation,
  animations,
  arrow,
  breakpoint,
  color,
  font,
  layers,
  mainBackground,
  modularScale,
  width,
};

export default GlobalTheme;

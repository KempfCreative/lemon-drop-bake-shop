import React, { useState, useEffect } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';
import { useCart } from "use-cart";
import { formatMoney } from 'accounting-js';
import { getProductWithParams } from '../../api/cms/product';
import { getOrderWithParams } from '../../api/cms/order';
import Typography from '../typography';

const globalAny:any = global;

interface StepProps {
  username: string
  orderDbId: number
  orderHash: string
}

const ConfirmWrapper = styled.section`
  width: 90%;
  margin: 0 auto;
`;

const CartListItemTitle = styled(Typography)`
  text-align: center;
`;

const OrderConfirmP = styled.p`
  text-align: center;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.brandPurple};
`;

const ConfirmLink = styled.a`
  color: ${({ theme }) => theme.color.brandPurple};
  text-decoration: none;
`;

const ConfirmStep: React.FC<StepProps> = ({ username, orderDbId, orderHash }) => {
  const { clearCart } = useCart();

  useEffect(() => {
    clearCart();
    globalAny.document.cookie = `orders=''; Max-Age=0; path=/`;
    globalAny.document.cookie = `orderDbId=''; Max-Age=0; path=/`;
    globalAny.document.cookie = `orderHash=''; Max-Age=0; path=/`;
    globalAny.document.cookie = `cart=''; Max-Age=0; path=/`;
  }, [])
  return (
    <ConfirmWrapper>
      <CartListItemTitle data={{type: "subheading", value:`Order #${orderHash} Confirmed!`}} />
      <OrderConfirmP>Thank you! Suzanne will be getting in touch to set up any other details, such as delivery or pick-up!</OrderConfirmP>
      <OrderConfirmP>If you have any other questions, please don't hesitate to contact us at 716-523-6160 or <ConfirmLink href="mailto:hellolemondropbakeshop@gmail.com">hellolemondropbakeshop@gmail.com</ConfirmLink>.</OrderConfirmP>
    </ConfirmWrapper>
  );
}

export default ConfirmStep;

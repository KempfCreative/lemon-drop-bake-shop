import React, { useEffect, useRef } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';
import { useCart } from "use-cart";
import Typography from '../typography';

const globalAny:any = global;

interface StepProps {
  username: string
  orderDbId: number
  orderHash: string
}

const CartLink = styled.a`
  position: relative;
  width: 6rem;
`;

const CartCount = styled.p`
  width: 3rem;
  height: 3rem;
  top: -0.5rem;
  right: -1rem;
  position: absolute;
  background: ${({ theme }) => theme.color.brandPurpleDark};
  color: ${({ theme }) => theme.color.white};
  padding: 0.3rem 0 0 1.1rem;
  border-radius: 50%;
  z-index: ${({ theme }) => theme.layers.upstage};
`;

const CartIcon = styled.img`
  position: absolute;
  display: block;
  width: 5rem;
  transition: transform 0.5s;
  :hover {
    transform: rotate(10deg);
  }
  &.--wiggle {
    animation: ${({ theme }) => theme.animations.wiggle} ${({ theme }) => theme.animation.default};
  }
`;

const Cart: React.FC<StepProps> = ({ username, orderDbId, orderHash }) => {
  const cartRef = useRef<HTMLImageElement>(null);

  const {
    addItem,
    removeItem,
    removeLineItem,
    clearCart,
    items,
    lineItemsCount
  } = useCart();

  useEffect(() => {
    globalAny.document.cookie = `cart=${JSON.stringify(items)}; path=/`;
    const elem = cartRef?.current;
    elem.classList.add('--wiggle');
    setTimeout(() => {
      elem.classList.remove('--wiggle');
    }, 1000);
  }, [items])
  return (
    <Link href="/cart" passHref>
      <CartLink>
        <CartCount>{lineItemsCount}</CartCount>
        <CartIcon ref={cartRef} src={`${process.env.NEXT_PUBLIC_CLIENT_HOST}/images/cart.svg`} alt="Grocery Basket Icon" />
      </CartLink>
    </Link>
  );
}

export default Cart;

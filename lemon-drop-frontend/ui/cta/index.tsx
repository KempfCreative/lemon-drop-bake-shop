import React from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';

const CtaWrap = styled.div`
  width: 100%;
  margin: ${({ theme}) => theme.modularScale.large} auto;
`;

const ActionLink = styled.a`
  background: ${({ theme }) => theme.color.brandPurpleDarker};
  color: ${({ theme }) => theme.color.mint};
  display: block;
  text-decoration: none;
  text-align: center;
  padding: ${({ theme}) => theme.modularScale.base} ${({ theme}) => theme.modularScale.xlarge};
  margin: 0 auto;
  width: 90%;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    width: 50%;
  }
`;

const Cta = ({ data }) => (
  <CtaWrap>
    <Link href={data.url} passHref>
      <ActionLink href={data.url}>
        <p>{data.text}</p>
      </ActionLink>
    </Link>
  </CtaWrap>
);

export default Cta;

import React from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled, { css } from 'styled-components';

const HeroWrapper = styled.section`
  position: relative;
  width: 90%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    display: block;
  }
`;

const HeadingSection = styled.div<{pos: {small: string, large: string}}>`
  padding: 1rem;
  position: relative;
  background-color: white;
  text-align: center;
  ${({ pos }) => pos.small === 'top' && css`
    order: -1;
  `}
  ${({ pos }) => pos.small === 'bottom' && css`
    order: 99;
  `}
  ${({ pos }) => pos.small === 'right' && css`
    top: 50%;
    bottom: unset;
    right: 0;
    left: unset;
    transform: translate(0, -50%);
  `}
  ${({ pos }) => pos.small === 'left' && css`
    top: 50%;
    bottom: unset;
    right: unset;
    left: 0;
    transform: translate(0, -50%);
  `}
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    position: absolute;
    padding: 2rem;
    max-width: 50%;
    ${({ pos }) => pos.large === 'top' && css`
      top: 0;
      bottom: unset;
      right: unset;
      left: 50%;
      transform: translate(-50%, 0);
    `}
    ${({ pos }) => pos.large === 'bottom' && css`
      top: unset;
      bottom: 0;
      right: unset;
      left: 50%;
      transform: translate(-50%, 0);
    `}
    ${({ pos }) => pos.large === 'right' && css`
      text-align: right;
      top: 50%;
      bottom: unset;
      right: 0;
      left: unset;
      right: 0;
      transform: translate(0, -50%);
    `}
    ${({ pos }) => pos.large === 'left' && css`
      text-align: left;
      top: 50%;
      bottom: unset;
      right: unset;
      left: 0;
      transform: translate(0, -50%);
    `}
  }
`;

const LargeHeading = styled.h1`
  font-family: ${({ theme }) => theme.font.bold};
  font-size: ${({ theme }) => theme.modularScale.large};
  color: ${({ theme }) => theme.color.brandPurpleDark};
  font-weight: bold;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    font-size: ${({ theme }) => theme.modularScale.xlarge};
  }
`;

const SmallHeading = styled.h2`
  font-family: ${({ theme }) => theme.font.bold};
  font-size: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.brandPurpleDark};
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    font-size: ${({ theme }) => theme.modularScale.large};
  }
`;

const HeroPic = styled.picture`
  width: 100%;
  height: auto;
`;

const HeroImage = styled.img`
  width: 100%;
`;

const CtaLink = styled.a`
  background: ${({ theme }) => theme.color.brandPurpleDarker};
  color: ${({ theme }) => theme.color.mint};
  display: inline-block;
  text-decoration: none;
  padding: ${({ theme}) => theme.modularScale.base} ${({ theme}) => theme.modularScale.xlarge};
  margin-top: ${({ theme}) => theme.modularScale.large};
`;

const Hero = ({ data }) => (
  <>
    {data.hero_content && (
      <HeroWrapper>
        <HeadingSection pos={{large: data.hero_content.large_position, small: data.hero_content.small_position}}>
          {data.hero_content?.heading ? (
            <LargeHeading>{data.hero_content.heading}</LargeHeading>
          ) : null }
          {data.hero_content?.subheading ? (
            <SmallHeading>{data.hero_content.subheading}</SmallHeading>
          ) : null }
          {data?.cta ? (
            <Link href={data.cta.url} passHref>
              <CtaLink>{data.cta.text}</CtaLink>
            </Link>
          ) : null }
        </HeadingSection>
        <HeroPic>
          <source
            media={'(max-width: 767px)'}
            srcSet={`${data.hero_content.small_image.url}`}
          />
          <source
            media={'(min-width: 768px)'}
            srcSet={`${data.hero_content.large_image.url}`}
          />
          <HeroImage src={`${data.hero_content.large_image.url}`} />
        </HeroPic>
      </HeroWrapper>
    )}
  </>
);

export default Hero;

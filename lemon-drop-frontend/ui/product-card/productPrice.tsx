import React, { useState } from 'react';
import styled, { css } from 'styled-components';
import { formatMoney } from 'accounting-js';
import Typography from '../typography';

const ProductPrice = styled(Typography)`
  font-size: ${({ theme }) => theme.modularScale.medium};
`;

const Price = (props) => {
  return (
    <ProductPrice
      data={{ type: "caption", value: formatMoney(props.price / 100) }}
    />
  );
};

export default Price;

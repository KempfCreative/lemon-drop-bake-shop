import React, { useState } from 'react';
import styled, { css } from 'styled-components';
import Typography from '../../ui/typography';

const ProductDl = styled.dl`
  padding: 2rem 0;
  display: flex;
  flex-direction: column;
`;

const ProductDetailGroup = styled.div``;

const ProductDetailTitle = styled.dt`
  font-style: italic;
  padding: 0 ${({ theme }) => theme.modularScale.medium} 0 0;
  float: left;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.brandPurple};
`;

const ProductDetailDef = styled.dd`
  float: left;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.brandPurple};
`;

const ProductDetails = (props) => {
  return (
    <>
      { props?.details.length > 0 ? (
        <ProductDl>
          {props.details.map((detail) => (
            <ProductDetailGroup key={detail.key} >
              <ProductDetailTitle>{detail.key}:</ProductDetailTitle>
              <ProductDetailDef>{detail.value}</ProductDetailDef>
            </ProductDetailGroup>
          ))}
        </ProductDl>
      ) : null }
    </>
  );
};

export default ProductDetails;

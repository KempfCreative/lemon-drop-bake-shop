import React, { useState } from 'react';
import styled, { css } from 'styled-components';

const ProductImage = styled.img<{isFull: boolean}>`
  width: 100%;
  max-height: 60rem;
  object-fit: contain;
  ${(props) => props.isFull && css`
    grid-column: 1;
    grid-row: -1/1;
  `}
`;

const ProductImg = (props) => {
  return (
    <>
      {props.asset.map((productImage) => (
        <ProductImage
          key={productImage.hash}
          isFull={props.isFull}
          src={`${productImage.url}`}
        />
      ))}
    </>
  );
};

export default ProductImg;

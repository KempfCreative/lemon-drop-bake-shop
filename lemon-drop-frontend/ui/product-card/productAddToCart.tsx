import React, { useState, useEffect, useCallback, useRef } from 'react';
import dynamic from 'next/dynamic';
import styled, { css } from 'styled-components';
import { useCart } from 'use-cart';
import crudOrder from '../../utils/create-update-order';
import Typography from '../typography';
import AddToCartButton from './addToCart';

const globalAny:any = global;

const ProductAddToCart = (props) => {
  const [orderDbId, setOrderDbId] = useState(0);
  const [atcText, setAtcText] = useState(props.single ? 'Add to cart' : 'Add 12 to cart');
  const [atcAmount, setAtcAmount] = useState(props.single ? 1 : 12);
  const [personalization, setPersonalization] = useState(null);
  const [templates, setTemplates] = useState([]);
  const [bundleLimit, setBundleLimit] = useState(12);
  const [disabled, setDisabled] = useState(false);
  const personalizationCallback = (data, limit) => {
    setPersonalization(data);
    setBundleLimit(limit);
  };
  const { addItem, isInCart } = useCart();

  useEffect(() => {
    if (props.product.Personalization?.length > 0) {
      const PersonalizationTemplates = props.product.Personalization.map((p) => {
        const [path, item] = p.__component.split('.');
        return {
          PersonalizationTemplate: dynamic(() => import(`../../${path}/${item}/index.tsx`)),
          personalizationProps: p
        };
      });
      setTemplates(PersonalizationTemplates);
    }
  }, []);
  
  useEffect(() => {
    if (bundleLimit < atcAmount) {
      setDisabled(true);
      setAtcText('Finish Personalizing This Order To Finish');
    } else {
      setDisabled(false);
      setAtcText(props.single ? 'Add to cart' : 'Add 12 to cart');
    }
  }, [bundleLimit]);

  const handleAddToCart = async (e) => {
    e.preventDefault();
    addItem(`${props.product.slug}`, atcAmount);
    setAtcText('Adding to cart...');
    let productDataDefault = { sku: props.product.slug, quantity: atcAmount, username: props.username };
    if (props.product?.Personalization?.length > 0) {
      productDataDefault = { ...personalization, ...productDataDefault };
    }
    const { id, orderId } = await crudOrder(props.orderHash, productDataDefault);
    setAtcText('Added to cart!');
    setTimeout(() => {
      setAtcText(props.single ? 'Add one more to cart' : 'Add 12 more to cart');
    }, 1000);
    setOrderDbId(id);
    globalAny.document.cookie = `orderDbId=${id}; path=/`;
    globalAny.document.cookie = `orderHash=${orderId}; path=/`;
  }

  return (
    <>
      {templates.length > 0 && props.isFull ?
        templates.map(({PersonalizationTemplate, personalizationProps}) => 
          <PersonalizationTemplate props={personalizationProps} passPersonalizationData={personalizationCallback} />)
        : null 
      }
      {/*<button onClick={() => personalizationCallback({}, bundleLimit - 1)} type="button">Click me</button> */}
      <AddToCartButton
        onClick={handleAddToCart}
        disabled={disabled}
        atcAmount={atcAmount}
        text={atcText}
      />
    </>
  )
};

export default ProductAddToCart;

import React, { useState } from 'react';
import styled, { css } from 'styled-components';
import Typography from '../../ui/typography';
import { secondsToDays } from '../../utils/secondsToString';

const ProductTitle = styled(Typography)`
`;

const LeadTime = styled(Typography)`
`;

const ProductHeader = (props) => {
  return (
    <>
      { props?.title ? (
        <ProductTitle
          data={{ type: "subheading", value: props.title }}
        />
      ) : null }
      { props?.category?.leadTime ? (
        <LeadTime
          data={{ type: "caption", value: secondsToDays(props.category.leadTime) }}
        />
      ) : null }
    </>
  )
};

export default ProductHeader;

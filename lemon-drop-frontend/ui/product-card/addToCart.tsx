import React, { useState } from 'react';
import styled, { css } from 'styled-components';

const AddToCart = styled.button`
  margin: ${({ theme }) => theme.modularScale.medium} 0;
  padding: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.white};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-family: ${({ theme }) => theme.font.body};
  width: 100%;
  ${(props) => props.disabled ? css`
    cursor: not-allowed;
    background: ${({ theme }) => theme.color.neutral};
  ` : css`
    cursor: pointer;
    background: ${({ theme }) => theme.color.brandPurple};
  `}
`;

const AddToCartButton = (props) => (
  <AddToCart 
    disabled={props.disabled} 
    onClick={props.onClick}
  >
  {props.text}
  </AddToCart>
);

AddToCartButton.whyDidYouRender = true;

export default AddToCartButton;

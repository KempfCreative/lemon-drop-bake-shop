import React, { useState } from 'react';
import styled, { css } from 'styled-components';
import Typography from '../../ui/typography';

const ProductDesc = styled(Typography)`
  text-align: left;
`;

const ProductDescription = (props) => {
  return (
    <>
      { props?.desc ? (
        <ProductDesc
          data={{type: "paragraph", value: props.desc}}
        />
      ) : null }
    </>
  );
};

export default ProductDescription;
import React, { useState, useEffect, useCallback } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import dynamic from 'next/dynamic';
import styled, { css } from 'styled-components';
import { formatMoney } from 'accounting-js';
import { useCart } from 'use-cart';
import crudOrder from '../../utils/create-update-order';
import { secondsToDays } from '../../utils/secondsToString';
import Bundle from '../../taxonomy/bundle';
import Typography from '../typography';
import ProductImg from './productImg';
import ProductHeader from './productHeader';
import ProductDescription from './productDescription';
import ProductDetails from './productDetails';
import ProductPrice from './productPrice';
import ProductAddToCart from './productAddToCart';

const globalAny:any = global;

const ProductWrapper = styled.section<{isFull: boolean}>`
  ${(props) => props.isFull && css`
    display: flex;
    flex-direction: column;
    width: 90%;
    margin: 0 auto;
    @media screen and (min-width: ${(props) => props.theme.breakpoint.small}) {
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-template-rows: repeat(auto-fit);
    }
  `}
  ${(props) => !props.isFull && css`
    flex-direction: column;
  `}
`;

const ProductInfoWrap = styled.div`
  grid-column: 2;
  display: flex;
  flex-direction: column;
`;

const AnchorWrap = styled.a`
  text-decoration: none;
  cursor: pointer;
`;

const ProductCard = ({ single = false, product, orderHash, username }) => (
  <ProductWrapper isFull={product.full}>
    { product.full ? (
      <>
        <ProductImg
          asset={product.Asset}
          isFull={product.full}
        />
        <ProductInfoWrap>
          <ProductHeader
            title={product.Title}
            category={product.category}
          />
          <ProductDescription
            desc={product.Description}
          />
          <ProductDetails
            details={product.details}
          />
          <ProductPrice 
            price={product.Price}
          />
          <ProductAddToCart
            single={single}
            product={product}
            orderHash={orderHash}
            username={username}
            isFull={product.full}
          />
        </ProductInfoWrap>
      </>
    ) : (
      <Link href={`/product/[name]`} as={`/product/${product.slug}`} passHref>
        <AnchorWrap>
          <ProductImg
            asset={product.Asset}
            isFull={product.full}
          />
          <ProductHeader
            title={product.Title}
            category={product.category}
          />
          <ProductPrice 
            price={product.Price}
          />
          <ProductAddToCart
            single={single}
            product={product}
            orderHash={orderHash}
            username={username}
            isFull={product.full}
          />
        </AnchorWrap>
      </Link>
    )}
  </ProductWrapper>
);

ProductCard.whyDidYouRender = true;

export default ProductCard;

import React, { useState, useEffect } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';
import { useCart } from "use-cart";
import { formatMoney } from 'accounting-js';
import { getProductWithParams } from '../../api/cms/product';
import { getOrderWithParams, putOrder } from '../../api/cms/order';
import Typography from '../typography';

const globalAny:any = global;

const Loading = styled.div`
  bottom: 0;
  left: 0;
  opacity: 0.75;
  pointer-events: none;
  position: fixed;
  right: 0;
  top: 0;
  visibility: visible;

  &:after {
    animation: spin .5s infinite linear;
    border: 0.25rem solid black;
    border-radius: 50%;
    border-right-color: transparent;
    border-top-color: transparent;
    content: '';
    display: inline-block;
    height: 2rem;
    left: calc(50% - (2rem / 2));
    position: absolute;
    top: calc(50% - (2rem / 2));
    width: 2rem;

    @keyframes spin {
      from { transform: rotate(0) }
      to { transform: rotate(359deg) }
    }
  }
`;

const CartList = styled.ul`
  width: 90%;
  margin: 0 auto;
  border-top: 2px solid ${({ theme }) => theme.color.mint};
  border-right: 2px solid ${({ theme }) => theme.color.mint};
  border-left: 2px solid ${({ theme }) => theme.color.mint};
`;

const CartListItem = styled.li`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1rem;
  border-bottom: 2px solid ${({ theme }) => theme.color.mint};
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    flex-direction: row;
  }
`;

const CartListItemImage = styled.img`
  max-height: 5rem;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    max-height: 10rem;
  }
`;

const CartListItemTitle = styled(Typography)`
  text-align: center;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    text-align: left;
    padding-left: 2rem;
  }
`;

const CartListItemQuantity = styled.dl`
  display: flex;
  align-items: center;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    padding-left: 2rem;
  }
`;

const Quantity = styled.dt`
  text-align: left;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.base};
  color: ${({ theme }) => theme.color.brandPurple};
  font-style: italic;
`;

const Minus = styled.button`
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.large};
  font-weight: bold;
  line-height: 2rem;
  cursor: pointer;
  margin-right: 1rem;
  color: ${({ theme }) => theme.color.brandPurpleDarker};
  border: 1px dotted ${({ theme }) => theme.color.brandPurpleDarker};
  background: transparent;
  margin-left: 1rem;
  padding: 0 .6rem;
`;

const Plus = styled.button`
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.large};
  font-weight: bold;
  line-height: 2rem;
  cursor: pointer;
  margin-left: 1rem;
  color: ${({ theme }) => theme.color.brandPurpleDarker};
  border: 1px dotted ${({ theme }) => theme.color.brandPurpleDarker};
  background: transparent;
  padding: 0 .6rem;
`;

const QuantityVal = styled.dd`
  text-align: left;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.brandPurple};
`;

const CartListItemPrice = styled.dl`
  display: flex;
  align-items: center;
  padding: 1rem 0;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    padding: 0 0 0 2rem;
  }
`;

const Price = styled.dt`
  text-align: left;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.base};
  color: ${({ theme }) => theme.color.brandPurple};
  font-style: italic;
`;

const PriceVal = styled.dd`
  text-align: left;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.brandPurple};
  padding-left: 1rem;
`;

const RemoveItem = styled.button`
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-style: italic;
  color: ${({ theme }) => theme.color.brandPurpleDarker};
  cursor: pointer;
  background: ${({ theme }) => theme.color.mint};
  padding: ${({ theme}) => theme.modularScale.base} ${({ theme}) => theme.modularScale.xlarge};
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    margin-left: auto;
  }
`;

const CartTotal = styled.li``;

const CartTotalVal = styled(Typography)``;

const CartActions = styled.li`
  display: flex;
  align-items: center;
  justify-content: center;
  border-bottom: 2px solid ${({ theme }) => theme.color.mint};
  padding-bottom: ${({ theme }) => theme.modularScale.medium};
`;

const RemoveAll = styled.button`
  margin: 0 ${({ theme}) => theme.modularScale.base};
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-style: italic;
  color: ${({ theme }) => theme.color.brandPurpleDarker};
  cursor: pointer;
  background: ${({ theme }) => theme.color.mint};
  padding: ${({ theme}) => theme.modularScale.base} ${({ theme}) => theme.modularScale.xlarge};
`;

const CheckoutCart = styled.a`
  margin: 0 ${({ theme}) => theme.modularScale.base};
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-style: italic;
  text-decoration: none;
  color: ${({ theme }) => theme.color.white};
  background: ${({ theme }) => theme.color.brandPurpleDarker};
  padding: ${({ theme}) => theme.modularScale.base} ${({ theme}) => theme.modularScale.xlarge};
`;

const CartItems = ({orderId}) => {
  const [cartItems, setCartItems] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [itemsLoaded, setItemsLoaded] = useState(false);

  const {
    items,
    addItem,
    removeItem,
    removeLineItem,
    clearCart
  } = useCart();

  useEffect(() => {
    const fetchProducts = async () => {
      const itemsPromises = items.map((item) => getProductWithParams({ name: 'slug', value: item.sku }));
      const itemsWithData = await Promise.all(itemsPromises).then((ip) => {
        return ip.map((product) => {
          const cartData = items.filter((cart) => cart.sku === product[0].slug);
          return Object.assign({}, cartData[0], product[0]);
        })
      });
      await putOrder({id: orderId, body: {cart: itemsWithData}});
      return itemsWithData;
    };
    const fetched = fetchProducts().then((data) => {
      const prices = data.map((d) => d.Price * d.quantity);
      const total = prices.reduce((t, p) => p + t ,0);
      setTotalPrice(total);
      setCartItems(data);
      setItemsLoaded(true);
    });
  }, [items]);

  return (
    <CartList>
      {itemsLoaded ? (
        <>
          {cartItems.length > 0 ? (
            <>
              {cartItems.map((item) => (
                <CartListItem key={item.sku}>
                  <CartListItemImage src={`${item.Asset[0].url}`} alt={item.Asset[0].name} />
                  <CartListItemTitle data={{type:"subheading", value:item.Title}} />
                  <CartListItemQuantity>
                    <Quantity>Quantity:</Quantity>
                    <Minus onClick={() => removeItem(item.sku, 12)}>-</Minus>
                    <QuantityVal>{item.quantity}</QuantityVal>
                    <Plus onClick={() => addItem(item.sku, 12)}>+</Plus>
                  </CartListItemQuantity>
                  <CartListItemPrice>
                    <Price>Price:</Price>
                    <PriceVal>{formatMoney(item.Price*item.quantity / 100)}</PriceVal>
                  </CartListItemPrice>
                  <RemoveItem onClick={() => removeLineItem(item.sku)} type="button">Remove Item</RemoveItem>
                </CartListItem>
              ))}
              <CartTotal>
                <CartTotalVal data={{type:"subheading", value:`Total Price: ${formatMoney(totalPrice / 100)}`}}/>
              </CartTotal>
              <CartActions>
                <RemoveAll onClick={() => clearCart()} type="button">Clear Cart</RemoveAll>
                <CheckoutCart href="/checkout/address">Checkout</CheckoutCart>
              </CartActions>
            </>
          ) : (
            <CartListItem>
              <CartListItemTitle data={{type: "subheading", value:"No Items In Cart"}} />
            </CartListItem>
          )}
        </>
      ) : (
        <Loading />
      )}
    </CartList>
  );
}

export default CartItems;

import React, { useState, useEffect } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';
import cookies from 'next-cookies';

interface AccountProps {
  data: {
    jwt: string
    username: string
  }
}

const AccountLink = styled.a`
  margin: 1rem 0;
  padding: 0.5rem 1rem;
  text-decoration: none;
  text-transform: uppercase;
  color: ${({ theme }) => theme.color.brandPurpleDarker};
  font-weight: bold;
  font-size: ${({ theme }) => theme.modularScale.base};
  &:hover {
    color: ${({ theme }) => theme.color.brandPurpleDark};
    text-decoration: underline;
  }
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    padding: 1rem 3rem;
    font-size: ${({ theme }) => theme.modularScale.medium};
  }
`;

const Account: React.FC<AccountProps> = ({ data }) => {
  const [loggedIn, setLoggedIn] = useState(false);
  const [userName, setUserName] = useState('Guest');

  useEffect(() => {
    if (data?.jwt?.length > 0) {
      setLoggedIn(true);
      setUserName(data?.username);
    }
  }, []);

  return (
    <>
      { loggedIn ? (
        <Link href='/account' passHref>
          <a href="/account" className="accountLink">{`Hi ${userName}`}</a>
        </Link>
      ) : (
        <Link href='/login' passHref>
          <a href="/login" className="accountLink">Login</a>
        </Link>
      )}
    </>
  );
};

export default Account;

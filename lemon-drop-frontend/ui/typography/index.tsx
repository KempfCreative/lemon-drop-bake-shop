import React from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';

const Heading = styled.h1`
  text-align: center;
  padding: 2rem 0;
  font-family: ${({ theme }) => theme.font.bold};
  font-size: ${({ theme }) => theme.modularScale.large};
  color: ${({ theme }) => theme.color.brandPurpleDark};
  font-weight: bold;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    padding: 4rem 0;
    font-size: ${({ theme }) => theme.modularScale.xlarge};
  }
`;

const Subheading = styled.h2`
  text-align: center;
  padding: 1rem 0;
  font-family: ${({ theme }) => theme.font.bold};
  font-size: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.brandPurpleDark};
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    padding: 2rem 0;
    font-size: ${({ theme }) => theme.modularScale.large};
  }
`;

const Paragraph = styled.p`
  text-align: center;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.brandPurple};
`;

const Caption = styled.p`
  text-align: center;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.base};
  color: ${({ theme }) => theme.color.brandPurple};
  font-style: italic;
`;

const renderType = ({ data: {type, value}, ...others }) => {
  switch (type) {
    case 'heading':
      return <Heading {...others} dangerouslySetInnerHTML={{ __html: value }} />;
    case 'subheading':
      return <Subheading {...others} dangerouslySetInnerHTML={{ __html: value }} />;
    case 'paragraph':
      return <Paragraph {...others} dangerouslySetInnerHTML={{ __html: value }} />;
    case 'caption':
      return <Caption {...others} dangerouslySetInnerHTML={{ __html: value }} />;
    default:
      return <span>{value}</span>;
  }
};

const Typography = (data) => renderType(data);

export default Typography;

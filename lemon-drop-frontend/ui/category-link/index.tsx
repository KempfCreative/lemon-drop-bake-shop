import React, { useState, useEffect } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';
import Typography from '../typography';

// this should display on one line if its siblings also are categorylinks

const CategoryWrapper = styled.section`
  width: 50%;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    width: 25%;
  }
`;

const CategoryImg = styled.img`
  width: 100%;
`;

const CategoryLink = ({ data }) => {
  const [catLink, setCatLink] = useState('');

  useEffect(() => {
    const defaultLink = data?.linkOverride ?? data?.categoryLinkCta?.url;
    setCatLink(defaultLink);
  }, []);

  return (
    <CategoryWrapper>
      <Link href={catLink} passHref>
        <a href={catLink}>
          { data?.icon?.url ? (
            <CategoryImg src={`${data.icon.url}`} />
          ) : null }
          { data?.displayTitle ? (
            <Typography data={{type: "heading", value: data.displayTitle}} />
          ) : null }
          { data?.categoryLinkCta ? (
            <Typography data={{type: "paragraph", value: data.categoryLinkCta.text}} />
          ) : null }
        </a>
      </Link>
    </CategoryWrapper>
  );
};

export default CategoryLink;

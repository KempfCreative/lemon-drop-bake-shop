import React from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';

const FloatedSection = styled.section`
  width: 90%;
  margin: ${({ theme }) => theme.modularScale.medium} auto;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    margin: ${({ theme }) => theme.modularScale.large} auto;
    flex-direction: row;
    flex-wrap: nowrap;    
  }
`;

const FloatedImg = styled.img`
  object-fit: cover;
  max-width: 100%;
`;

const FloatedText = styled.p`
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.brandPurple};
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    padding: ${({ theme }) => theme.modularScale.medium};
  }
`;

const FloatedParagraph = ({ data }) => (
  <FloatedSection>
    {/* The data.mask element will have to be a TODO https://css-tricks.com/clipping-masking-css/ */}
    <FloatedImg src={`${data.image.url}`} />
    <FloatedText dangerouslySetInnerHTML={{ __html: data.text }} />
  </FloatedSection>
);

export default FloatedParagraph;

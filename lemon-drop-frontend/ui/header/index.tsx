import React, { useState } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';

const PageHeader = styled.header`
  margin: -5rem auto 0 auto;
  width: 15rem;
`;

const LogoImage = styled.img`
  height: 10rem;
  // TODO Get the elements to float next to the clip path or shape outside
  // shape-outside: url(images/lemon-drop-logo.png);
  // float: left;
`;

const LargeHeading = styled.h1`
  font-family: ${({ theme }) => theme.font.head};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-style: italic;
  font-weight: bold;
  color: ${({ theme }) => theme.color.brandPurpleDark};
  width: 100%;
  word-spacing: 100%;
  padding: 0.8rem 0 0 1rem;
  line-height: 1.2;
`;

const LogoWrapper = styled.a`
  display: flex;
  cursor: pointer;
  transition: transform 0.5s;
  :hover {
    transform: rotate(5deg);
  }
`;

const Header = () => (
  <header className="pageHeader">
    <Link href="/" passHref>
      <a className="pageHeader__logoWrapper" href="/">
        <img className="pageHeader__logoImage" src={`${process.env.NEXT_PUBLIC_CLIENT_HOST}/images/lemon-drop-logo.svg`} alt="Icon of a lemon drop martini with a wisk as a mixer." />
        <h1 className="pageHeader__logoText">Lemon Drop Bake .Shop</h1>
      </a>
    </Link>
  </header>
);

export default Header;

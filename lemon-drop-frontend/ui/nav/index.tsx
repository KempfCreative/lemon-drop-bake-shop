import React, { useState, useEffect } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';

const Nav = (props) => {
  const [navLinks, setNavLinks] = useState(props.nav);
  return (
    <nav className="pageNav">
      { navLinks?.length > 0 && (
        <ul className="pageNav__listWrapper">
          {navLinks.map((n) => (
            <li className="pageNav__listWrapper-item" key={`${n.uri}-${n.title}`}>
              <Link href={`/shop/[type]`} as={n.uri} passHref>
                <a className="pageNav__listWrapper-link">
                  {n.title}
                </a>
              </Link>
            </li>
          ))}
        </ul>
      )}
    </nav>
  );
};

export default Nav;

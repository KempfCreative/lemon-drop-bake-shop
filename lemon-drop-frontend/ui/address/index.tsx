import React, { useState, useEffect } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import Router from 'next/router';
import cookies from 'next-cookies';
import styled, { css } from 'styled-components';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useCart } from "use-cart";
import { formatMoney } from 'accounting-js';
import { getProductWithParams } from '../../api/cms/product';
import { getOrderWithParams, putOrder } from '../../api/cms/order';
import Typography from '../typography';

interface StepProps {
  username: string
  orderDbId: number
  orderHash: string
}

const Form = styled.form`
  width: 90%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
`;

const InputWrap = styled.div`
  position: relative;
  display: grid;
  grid-gap: 1rem;
  margin-bottom: 3rem;
`;

const InputLabel = styled.label`
  font-family: ${({ theme }) => theme.font.body};
  font-style: italic;
  color: ${({ theme }) => theme.color.brandPurpleDarker};
`;

const InputField = styled.input<{error: string}>`
  border: 2px solid ${(props) => props.error ? props.theme.color.error : props.theme.color.brandPurple};
  font-size: ${({ theme }) => theme.modularScale.medium};
  padding: ${({ theme }) => theme.modularScale.medium};
  ::placeholder: {
    color: ${({ theme }) => theme.color.brandPurple};
  }
`;

const InputError = styled.span`
  font-family: ${({ theme }) => theme.font.body};
  font-style: italic;
  color: ${({ theme }) => theme.color.error};
  position: absolute;
  bottom: -2.5rem;
`;

const SelectReset = css`
  background: transparent;
  border-radius: 0;
  border: none;
  color: inherit;
  font: inherit;
  line-height: normal;
  margin: 0;
  overflow: visible;
  padding: 0;
  width: auto;
  -moz-osx-font-smoothing: inherit;
  appearance: none;
  -webkit-font-smoothing: inherit;
  :active {
    outline: 0;
  }
`;

const SelectField = styled.select<{error: string}>`
  ${SelectReset}
  ${({ theme }) => theme.arrow('down', theme.color.brandPurple, 'right calc(1.5rem - 5px) center')}
  border: 2px solid ${(props) => props.error ? props.theme.color.error : props.theme.color.brandPurple};
  font-size: ${({ theme }) => theme.modularScale.medium};
  padding: ${({ theme }) => theme.modularScale.medium};
  ::placeholder: {
    color: ${({ theme }) => theme.color.brandPurple};
  }
`;

const AddressButton = styled.button`
  margin: 0 auto 4rem auto;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-style: italic;
  text-decoration: none;
  color: ${({ theme }) => theme.color.white};
  cursor: pointer;
  background: ${({ theme }) => theme.color.brandPurpleDarker};
  padding: ${({ theme}) => theme.modularScale.base} ${({ theme}) => theme.modularScale.xlarge};
`;

const AddressForm: React.FC<StepProps> = ({username, orderDbId, orderHash}) => {
  const [states] = useState(['AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY']);
  useEffect(() => {
    const updateStatus = async () => {
      await putOrder({ id: orderDbId, body: {status: 'address'} });
    };
    updateStatus();
  }, []);

  const customSubmit = async (values, actions) => {
    const { id } = await getOrderWithParams({ name: "orderId", value: orderHash }).then(o => o.length > 0 ? o[0] : {id: 0});
    putOrder({ id, body: {status: 'payment', ...values} })
      .then((res) => {
        if (res.error) {
          actions.setSubmitting(false);
          actions.setStatus({ button: 'Address Wasn\'t Able To Confirm' })
          return;
        }
        actions.setSubmitting(false);
        actions.setStatus({ button: 'Address Confirmed' });
        actions.setStatus({ addressConfirmed: true });
        Router.push('/checkout/payment')
      })
      .catch((e) => {
        actions.setSubmitting(false);
        actions.setStatus({ button: 'Confirm Address' })
      })
  }
  return (
    <Formik
      initialValues={{
        customerName: '',
        email: '',
        phoneNumber: '',
        addressStreet: '',
        addressCity: '',
        addressState: ''
      }}

      initialStatus={{
        button: 'Confirm Address',
        addressConfirmed: false
      }}

      validationSchema={
        Yup.object().shape({
          customerName: Yup.string().required('Don\'t be shy, please tell us your name, so we know who to talk to'),
          email: Yup.string().email().required('We need your email, as we will be communicating with you about your order'),
          phoneNumber: Yup.number(),
          addressStreet: Yup.string().required('We need your address to make you baked goods'),
          addressCity: Yup.string().required('We currently only deliver within a very short radius of our business, so we need your city'),
          addressState: Yup.string().required('We are a NY based business, and most of our business operates in that state only.')
        })
      }

      onSubmit={async (values, actions) => {
        actions.setStatus({ button: 'Validating Address...' });
        customSubmit(values, actions);
      }}

      validateOnBlur={false}
      validateOnChange={false}
    >
      {(formikProps) => {
        return (
          <>
            <Form onSubmit={formikProps.handleSubmit}>
              <InputWrap>
                <InputLabel htmlFor="customer-name">Full Name</InputLabel>
                <InputField
                  name="customerName"
                  id="customer-name"
                  placeholder="Lets be on a first name basis"
                  type="text"
                  onChange={formikProps.handleChange}
                  value={formikProps.values.customerName}
                  error={formikProps.errors.customerName}
                />
                {formikProps.errors.customerName && <InputError>{formikProps.errors.customerName}</InputError>}
              </InputWrap>

              <InputWrap>
                <InputLabel htmlFor="email">Email Address</InputLabel>
                <InputField
                  name="email"
                  id="email"
                  placeholder="cupcakelover@email.com"
                  type="text"
                  onChange={formikProps.handleChange}
                  value={formikProps.values.email}
                  error={formikProps.errors.email}
                />
                {formikProps.errors.email && <InputError>{formikProps.errors.email}</InputError>}
              </InputWrap>

              <InputWrap>
                <InputLabel htmlFor="phone-number">Phone Number</InputLabel>
                <InputField
                  name="phoneNumber"
                  id="phone-number"
                  placeholder="7165555555"
                  type="tel"
                  onChange={formikProps.handleChange}
                  value={formikProps.values.phoneNumber}
                  error={formikProps.errors.phoneNumber}
                />
                {formikProps.errors.phoneNumber && <InputError>{formikProps.errors.phoneNumber}</InputError>}
              </InputWrap>

              <InputWrap>
                <InputLabel htmlFor="address-street">Street Address</InputLabel>
                <InputField
                  name="addressStreet"
                  id="address-street"
                  placeholder="55 Main Street"
                  type="text"
                  onChange={formikProps.handleChange}
                  value={formikProps.values.addressStreet}
                  error={formikProps.errors.addressStreet}
                />
                {formikProps.errors.addressStreet && <InputError>{formikProps.errors.addressStreet}</InputError>}
              </InputWrap>

              <InputWrap>
                <InputLabel htmlFor="address-city">City</InputLabel>
                <InputField
                  name="addressCity"
                  id="address-city"
                  placeholder="Buffalo"
                  type="text"
                  onChange={formikProps.handleChange}
                  value={formikProps.values.addressCity}
                  error={formikProps.errors.addressCity}
                />
                {formikProps.errors.addressCity && <InputError>{formikProps.errors.addressCity}</InputError>}
              </InputWrap>

              <InputWrap>
                <InputLabel htmlFor="address-state">State</InputLabel>
                <SelectField
                  name="addressState"
                  id="address-state"
                  placeholder="NY"
                  onChange={formikProps.handleChange}
                  value={formikProps.values.addressState}
                  error={formikProps.errors.addressState}
                >
                  {states.map((state) => (
                    <option value={state}>{state}</option>
                  ))}
                </SelectField>
                {formikProps.errors.addressState && <InputError>{formikProps.errors.addressState}</InputError>}
              </InputWrap>

              <AddressButton type="submit" disabled={formikProps.isSubmitting}>{formikProps.status.button}</AddressButton>
            </Form>
          </>
        )
      }}
    </Formik>
  )
};

export default AddressForm;

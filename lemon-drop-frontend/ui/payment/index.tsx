import React, { useState, useEffect } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import Router from 'next/router';
import styled from 'styled-components';
import * as fetchImport from 'isomorphic-unfetch'
import { formatMoney } from 'accounting-js';
import { useCart } from "use-cart";
import useScript from '../../utils/hooks/useScript';
import { getProductWithParams } from '../../api/cms/product';
import { getOrderWithParams, putOrder } from '../../api/cms/order';

const globalAny:any = global;
const fetch = (fetchImport.default || fetchImport) as typeof fetchImport.default

interface StepProps {
  username: string
  orderDbId: number
  orderHash: string
}

const appId = process.env.NODE_ENV === 'development' 
  ? process.env.NEXT_PUBLIC_SQUARE_SANDBOX_APP_ID
  : process.env.NEXT_PUBLIC_SQUARE_APP_ID;

const PaymentWrapper = styled.section`
  width: 90%;
  margin: 0 auto;
  .sq-input {
    border: 2px solid ${({ theme }) => theme.color.brandPurple};
    margin: 1rem 0;
  }
`;

const PayIframe = styled.div`
  display: flex;
  flex-direction: column;
`;

const PaymentButton = styled.button`
  margin: 0 auto;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-style: italic;
  text-decoration: none;
  color: ${({ theme }) => theme.color.white};
  cursor: pointer;
  background: ${({ theme }) => theme.color.brandPurpleDarker};
  padding: ${({ theme}) => theme.modularScale.base} ${({ theme}) => theme.modularScale.xlarge};
`;

const SquareImage = styled.img`
  width: 20rem;
  height: auto;
  margin: 3rem auto 0;
`;

const Payment: React.FC<StepProps> = ({ username, orderDbId, orderHash }) => {
  interface PayType { build<Function>(), requestCardNonce<Function>() }
  const [paymentForm, setPaymentForm] = useState<PayType>();
  const [totalPrice, setTotalPrice] = useState(0);

  const [loaded, error] = useScript(
    process.env.NODE_ENV === 'development' 
    ? "https://js.squareupsandbox.com/v2/paymentform" 
    : "https://js.squareup.com/v2/paymentform");

  const { items } = useCart();

  const paymentFormGlobal: any = paymentForm;

  useEffect(() => {
    const fetchProducts = async () => {
      const itemsPromises = items.map((item) => getProductWithParams({ name: 'slug', value: item.sku }));
      const itemsWithData = await Promise.all(itemsPromises).then((ip) => {
        return ip.map((product) => {
          const cartData = items.filter((cart) => cart.sku === product[0].slug);
          return Object.assign({}, cartData[0], product[0]);
        })
      });
      return itemsWithData;
    };
    const fetched = fetchProducts().then((data) => {
      const prices = data.map((d) => d.Price * d.quantity);
      const total = prices.reduce((t, p) => p + t ,0);
      setTotalPrice(total);
    });
  }, [items]);

  useEffect(() => {
    console.log('appId', appId);
    console.log('NEXT_PUBLIC_SQUARE_SANDBOX_APP_ID', process.env.NEXT_PUBLIC_SQUARE_SANDBOX_APP_ID);
    if (loaded && globalAny.SqPaymentForm && totalPrice) {
      const payForm = new globalAny.SqPaymentForm({
        // Initialize the payment form elements
        //TODO: Replace with your sandbox application ID
        applicationId: appId,
        inputClass: 'sq-input',
        autoBuild: false,
        // Customize the CSS for SqPaymentForm iframe elements
        inputStyles: [{
            fontSize: '18.75px',
            lineHeight: '23.438px',
            padding: '18.75px',
            placeholderColor: '#CFABF5',
            backgroundColor: 'transparent',
        }],
        // Initialize the credit card placeholders
        cardNumber: {
            elementId: 'sq-card-number',
            placeholder: 'Card Number'
        },
        cvv: {
            elementId: 'sq-cvv',
            placeholder: 'CVV'
        },
        expirationDate: {
            elementId: 'sq-expiration-date',
            placeholder: 'MM/YY'
        },
        postalCode: {
            elementId: 'sq-postal-code',
            placeholder: 'Postal Code'
        },
        // SqPaymentForm callback functions
        callbacks: {
          /*
            * callback function: cardNonceResponseReceived
            * Triggered when: SqPaymentForm completes a card nonce request
          */
          cardNonceResponseReceived: function (errors, nonce, cardData) {
            if (errors) {
              // Log errors from nonce generation to the browser developer console.
              console.error('Encountered errors:');
              errors.forEach(function (error) {
                console.error('  ' + error.message);
              });
              alert('Encountered errors, check browser developer console for more details');
              return;
            }
            fetch(`${process.env.NEXT_PUBLIC_CLIENT_HOST}/api/process-payment`, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  nonce: nonce,
                  amount: totalPrice
                })
              })
              .catch(err => {
                alert('Network error: ' + err);
              })
              .then((res: any) => {
                if (!res.ok) {
                  return res.json().then(errorInfo => Promise.reject(errorInfo));
                }
                return res.json();
              })
              .then(data => {
                putOrder({id: orderDbId, body: {status: 'confirm', paymentConfirmation: data}}).then(orderConfirmation => Router.push('/checkout/confirm'))
              })
              .catch(err => {
                console.error(err);
                alert('Payment failed to complete!\nCheck browser developer console for more details');
              });

          }
        }
      });
      setPaymentForm(payForm);
    }
  }, [loaded, totalPrice]);

  useEffect(() => {
    if (loaded && globalAny.SqPaymentForm) {
      paymentForm.build();
    }
  }, [paymentForm]);

  const onGetCardNonce = (event) => {
    event.preventDefault();
    paymentForm.requestCardNonce();
  };

  return (
    <PaymentWrapper>
      {loaded ? (
        <PayIframe id="form-container">
          <div id="sq-card-number"></div>
          <div className="third" id="sq-expiration-date"></div>
          <div className="third" id="sq-cvv"></div>
          <div className="third" id="sq-postal-code"></div>
          <PaymentButton id="sq-creditcard" className="button-credit-card" onClick={() => onGetCardNonce(event)}>{`Pay ${formatMoney(totalPrice / 100)}`}</PaymentButton>
          <SquareImage src='/images/square-powered-white-small.png' alt="Powered By Square" />
        </PayIframe>
      ) : (
        <p>Loading...</p>
      )}
    </PaymentWrapper>
  );
};

export default Payment;

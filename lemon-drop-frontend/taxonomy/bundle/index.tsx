// bundles should be a selection of products, and as each product is selected, the total number of options go down
// the products to be selected come from `singleProduct`, 
// and the number of each to be selected should come from the bundle Limit, not hte total limit of each product
import React, { useState, useEffect, useReducer, memo } from 'react';
import styled from 'styled-components';
import BundleItem from './bundleItem';
import Typography from '../../ui/typography';

const BundleTitle = styled(Typography)`
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    margin-bottom: -2.5rem;
    grid-column: 2;
  }
`;

const BundleList = styled.ul`
  width: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  padding-top: 3rem;
  border: 1px solid ${({ theme }) => theme.color.brandPurple};
  border-radius: 1rem;
  margin-bottom: 2rem;

  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    padding-top: 5rem;
    flex-direction: row;
    justify-content: center;
    grid-column: 2;
  }
`;

const Bundle = ({ props, passPersonalizationData }) => {
  const { Limit, singleProduct } = props;
  const numItems = Math.floor(Limit / singleProduct.length);
  const numRemaining = Limit % singleProduct.length;
  const initialState = props.singleProduct.reduce((allProduct, product, index) => {
    const {id} = product;
    if (index + 1 <= numRemaining) {
      product.bundleAmount = numItems + (numItems / numRemaining);
    } else {
      product.bundleAmount = numItems;
    }
    return { [id]: product, ...allProduct }
  }, {});

  function bundleItemReducer(state, action) {
    const {type, id} = action;
    switch (type) {
      case 'inc':
        return {
          ...state,
          [id]: {
            ...state[id],
            bundleAmount: state[id].bundleAmount + 1
          }
        }
      case 'dec':
        return {
          ...state,
          [id]: {
            ...state[id],
            bundleAmount: state[id].bundleAmount - 1
          }
        }
      default:
        return state;
    }
  }

  const [bundleItems, dispatch] = useReducer(bundleItemReducer, initialState);

  const [changeableLimit, setChangeableLimit] = useState(Limit);

  useEffect(() => {
    const numberOfItems = Object.keys(bundleItems).reduce((acc, cur) => {
      const num = bundleItems[cur].bundleAmount;
      return acc + num;
    }, 0)
    setChangeableLimit(numberOfItems);
    passPersonalizationData(bundleItems, numberOfItems);
  }, [bundleItems])

  return (
    <>
      <BundleTitle data={{type: "subheading", value: "Bundle Contains:"}} />
      <BundleList>
        {Object.keys(bundleItems).map((item) => 
          <BundleItem
            key={bundleItems[item].id}
            itemCount={bundleItems[item].bundleAmount}
            product={bundleItems[item].product}
            title={bundleItems[item].product.Title}
            onIncrease={changeableLimit < Limit ? () => dispatch({ type: 'inc', id: bundleItems[item].id }) : false}
            onReduce={bundleItems[item].bundleAmount > 0 ? () => dispatch({ type: 'dec', id: bundleItems[item].id }) : false}
          />
        )}
      </BundleList>
    </>
  );
}

export default memo(Bundle);

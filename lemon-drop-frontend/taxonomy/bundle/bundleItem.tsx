import React, { useState, useEffect, memo } from 'react';
import styled from 'styled-components';
import Typography from '../../ui/typography';


const BundleLi = styled.li`
  box-sizing: border-box;
  width: 100%;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    width: 25%;
    margin: 1rem 2rem;
  }
`;

const BundleItemImage = styled.img`
  width: 100%;
  padding: 0 2rem;
`;

const BundleItemTitle = styled(Typography)``;

const BundleForm = styled.form`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

const BundleItemTotal = styled.input`
  text-align: center;
  width: 2rem;
  flex-grow: 1;
  text-align: center;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  color: ${({ theme }) => theme.color.brandPurple};
`;

const BundleItemIncrement = styled.button`
  flex-grow: 1;
  margin: ${({ theme }) => theme.modularScale.small} 0;
  padding: ${({ theme }) => theme.modularScale.small};
  background: ${({ theme }) => theme.color.brandPurple};
  color: ${({ theme }) => theme.color.white};
  cursor: pointer;
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-family: ${({ theme }) => theme.font.body};
  &:disabled {
    background: ${({ theme }) => theme.color.neutral};
    opacity: .5;
    cursor: no-cursor;
  }
`;

const BundleItem = (props) => {
  return (
    <BundleLi>
      <BundleItemImage src={props?.product?.Asset[0]?.formats?.thumbnail?.url ?? props?.product?.Asset[0]?.url} />
      <BundleItemTitle
        data={{type: "paragraph", value: props.title}}
      />
      <BundleForm>
        <BundleItemIncrement 
          onClick={() => props.onIncrease()} 
          disabled={!props.onIncrease} 
          type="button"
        >
        ▲
        </BundleItemIncrement>
        <BundleItemTotal value={props.itemCount} />
        <BundleItemIncrement 
          onClick={() => props.onReduce()} 
          disabled={!props.onReduce} 
          type="button"
        >
        ▼
        </BundleItemIncrement>
      </BundleForm>
    </BundleLi>
  );
}

export default memo(BundleItem);
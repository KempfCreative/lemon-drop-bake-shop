import checkFetchResponse from '../../../utils/check-fetch-response';

const login = async (user) => {
  const { identifier, password } = user;

  const url = `${process.env.NEXT_PUBLIC_CMS_HOST}/auth/local`;

  const loginData = await global.fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(user)
  })
    .then(checkFetchResponse)
    .then((r) => {
      return r.json()
    })
    .then((d) => d)
    .catch((error) => {
      return error.response;
    });

  return loginData;
};

const register = async (user) => {
  const url = `${process.env.NEXT_PUBLIC_CMS_HOST}/auth/local/register`;

  const registerData = await global.fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(user)
  })
    .then(checkFetchResponse)
    .then((r) => {
      return r.json()
    })
    .then((d) => d)
    .catch((error) => {
      return error.response;
    });

  return registerData;
};

export { login, register };

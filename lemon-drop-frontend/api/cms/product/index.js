import checkFetchResponse from '../../../utils/check-fetch-response';

const getProduct = async () => {
  const url = `${process.env.NEXT_PUBLIC_CMS_HOST}/products`;

  const pages = await global.fetch(url)
    .then(checkFetchResponse)
    .then((r) => {
      return r.json()
    })
    .then((d) => d)
    .catch((error) => {
      return error.response;
    });

  return pages;
};

const getProductWithParams = async (param = {}) => {
  // TODO: Male the params amap so you can iterate
  const url = `${process.env.NEXT_PUBLIC_CMS_HOST}/products?${param.name}=${param.value}&Active=true`;

  const page = await global.fetch(url)
    .then(checkFetchResponse)
    .then((r) => {
      return r.json()
    })
    .then((d) => d)
    .catch((error) => {
      return error.response;
    });

  return page;
};

export { getProduct, getProductWithParams };

import checkFetchResponse from '../../../utils/check-fetch-response';

const getOrder = async (params = {}) => {
  const url = `${process.env.NEXT_PUBLIC_CMS_HOST}/order/${params.id}`;

  const order = await global.fetch(url)
  .then(checkFetchResponse)
  .then((r) => {
    return r.json()
  })
  .then((d) => d)
  .catch((error) => {
    return error.response;
  });

  return order;
};

const getOrderWithParams = async (param = {}) => {
  const url = `${process.env.NEXT_PUBLIC_CMS_HOST}/orders?${param.name}=${param.value}`;

  const order = await global.fetch(url)
  .then(checkFetchResponse)
  .then((r) => {
    return r.json()
  })
  .then((d) => d)
  .catch((error) => {
    return error.response;
  });

  return order;
};

const newOrder = async (param = {}) => {
  const url = `${process.env.NEXT_PUBLIC_CMS_HOST}/orders`;

  const order = await global.fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(param?.body ?? '')
  })
  .then(checkFetchResponse)
  .then((r) => {
    return r.json()
  })
  .then((d) => d)
  .catch((error) => {
    return error.response;
  });

  return order;
};

const putOrder = async (param = {}) => {
  const url = `${process.env.NEXT_PUBLIC_CMS_HOST}/orders/${param.id}`;

  const order = await global.fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(param?.body ?? '')
  })
  .then(checkFetchResponse)
  .then((r) => {
    return r.json()
  })
  .then((d) => d)
  .catch((error) => {
    return error.response;
  });

  return order;

}
export { getOrder, getOrderWithParams, newOrder, putOrder };

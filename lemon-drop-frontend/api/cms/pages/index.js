import checkFetchResponse from '../../../utils/check-fetch-response';

const getPages = async () => {
  const url = `${process.env.NEXT_PUBLIC_CMS_HOST}/pages`;

  const pages = await global.fetch(url)
    .then(checkFetchResponse)
    .then((r) => {
      return r.json()
    })
    .then((d) => d)
    .catch((error) => {
      return error.response;
    });

  return pages;
};

const getPageWithParams = async (param = {}) => {
  // TODO: Male the params amap so you can iterate
  const url = `${process.env.NEXT_PUBLIC_CMS_HOST}/pages?${param.name}=${param.value}`;

  const page = await global.fetch(url)
    .then(checkFetchResponse)
    .then((r) => {
      return r.json()
    })
    .then((d) => d)
    .catch((error) => {
      return error.response;
    });

  return page;
};

export { getPages, getPageWithParams };

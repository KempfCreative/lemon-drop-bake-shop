import styled from 'styled-components';
import Head from 'next/head';
import cookies from 'next-cookies';
import Header from '../ui/header';
import Account from '../ui/account';
import Cart from '../ui/cart';
import Typography from '../ui/typography';

const AccountPage = () => (
  <>
    <Typography data={{ type: "heading", value: "Account" }} />
    <Typography data={{ type: "paragraph", value: "This page is coming soon! Your order data and account data will be here shortly" }} />
  </>
);

export default AccountPage;

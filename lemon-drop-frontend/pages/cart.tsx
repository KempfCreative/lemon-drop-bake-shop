import { useState, useEffect } from 'react';
import styled from 'styled-components';
import Head from 'next/head';
import cookies from 'next-cookies';
import Header from '../ui/header';
import Account from '../ui/account';
import Cart from '../ui/cart';
import CartItems from '../ui/cart-items';
import Typography from '../ui/typography';

const Main = styled.main`
  width: 90vw;
  border: 5px solid ${({ theme }) => theme.color.neutral};
  border-radius: 2rem;
  background: ${({ theme }) => theme.color.white};
  max-width: 1400px;
  min-height: 80vh;
  margin: 10vh auto 10vh auto;
`;

const Links = styled.div`
  width: 95%;
  margin: 0 auto;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    margin: -2.5rem 2rem 2.5rem 2rem;
    display: flex;
    justify-content: space-between;
  }
`;

const UserWrapper = styled.div`
  position: fixed;
  bottom: 0;
  display: flex;
  justify-content: space-between;
  width: 100vw;
  margin-left: -8.5vw;
  background: ${({ theme }) => theme.color.white};
  padding-top: 1rem;
  z-index: ${({ theme }) => theme.layers.audience};
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    position: static;
    bottom: unset;
    justify-content: unset;
    width: unset;
    margin-left: 0;
    background: transparent;
    padding-top: 0;
    z-index: unset;
`;

const CartPage = ({orderDbId}) => (
  <>
    <Typography data={{ type: "heading", value: "My Cart" }} />
    <CartItems orderId={orderDbId} />
  </>
);

CartPage.getInitialProps = async (ctx) => {
  const { orderDbId = 0} = cookies(ctx);
  return { orderDbId };
}

export default CartPage;

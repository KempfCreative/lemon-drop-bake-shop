import React from 'react';
import { AppProps } from 'next/app';
import Head from 'next/head';
import styled, { ThemeProvider } from 'styled-components';
import cookies from 'next-cookies';
import { CartProvider } from "use-cart";
import GlobalTheme from '../theme/theme';
import GlobalStyles from '../theme/styles';
import { generateMeta } from '../utils/generateMeta';
import Header from '../ui/header';
import Nav from '../ui/nav';
import Account from '../ui/account';
import Cart from '../ui/cart';
import { getNavsWithParams } from './../api/cms/navigation';
if (process.env.NODE_ENV === 'development') {
  if (typeof window !== 'undefined') {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const whyDidYouRender = require('@welldone-software/why-did-you-render');
    whyDidYouRender(React, {
      trackAllPureComponents: true
    });
  }
}

const LemonDropApp = ({
  pageProps,
  Component, 
  navProps, 
  jwt, 
  username, 
  cart, 
  orderDbId, 
  orderHash
}) => {
  return (
    <>
      <Head>
        { generateMeta() }
      </Head>
      <GlobalStyles />
      <ThemeProvider theme={GlobalTheme}>
        <CartProvider initialCart={cart?.length > 0 ? cart : []}>
          <main className="appMain">
            <Header />
            <div className="appLinks">
              <Nav nav={navProps} />
              <div className="userWrapper">
                <Account data={{ jwt, username }} />
                <Cart username={username} orderDbId={orderDbId} orderHash={orderHash} />
              </div>
            </div>
            <Component {...pageProps} />
          </main>
        </CartProvider>
      </ThemeProvider>
    </>
  );
};

LemonDropApp.getInitialProps = async ({Component, ctx}) => {
  const { jwt = '', username = '', cart = [], orderDbId = 0, orderHash = '' } = cookies(ctx);
  const navRes = await getNavsWithParams({ name: 'active', value: true })
      .then(navs => navs);
  const initialProps = {
    pageProps: (
      Component.getInitialProps
        ? await Component.getInitialProps(ctx)
        : {}
    ),
    navProps: navRes,
    jwt,
    username,
    cart,
    orderDbId,
    orderHash
  }
  return initialProps;
}

LemonDropApp.whyDidYouRender = true;

export default LemonDropApp;

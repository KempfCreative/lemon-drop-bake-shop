const crypto = require('crypto');
const squareConnect = require('square-connect');

const appToken = process.env.NODE_ENV === 'development' 
  ? process.env.SQUARE_SANDBOX_ACCESS_TOKEN
  : process.env.SQUARE_ACCESS_TOKEN;

const defaultClient = squareConnect.ApiClient.instance;

const oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = appToken;

defaultClient.basePath = process.env.NODE_ENV === 'development'
  ? 'https://connect.squareupsandbox.com'
  : 'https://connect.squareup.com';

export default async (req, res) => {
  const {
    method,
    body
  } = req;

  switch (method) {
    case 'POST':
      const idempotency_key = crypto.randomBytes(22).toString('hex');
      const payments_api = new squareConnect.PaymentsApi();
      const request_body = {
        source_id: body.nonce,
        amount_money: {
          amount: body.amount, // $1.00 charge
          currency: 'USD'
        },
        idempotency_key: idempotency_key
      };
      try {
        const resp = await payments_api.createPayment(request_body);
        const json = JSON.stringify(resp);
        res.status(200).json(json);
      } catch (error) {
        console.error(error);
        res.status(502).json(error);
      }
      break
    default:
      res.setHeader('Allow', ['GET', 'PUT'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}

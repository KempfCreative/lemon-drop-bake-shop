
import React from 'react';
import * as Yup from 'yup';
import Router from 'next/router';
import Link from 'next/link';
import styled from 'styled-components';
import { Formik } from 'formik';
import cookies from 'next-cookies';
import { register, login } from '../api/cms/auth';
import Header from '../ui/header';
import Account from '../ui/account';
import Cart from '../ui/cart';

const Form = styled.form`
  width: 90%;
  margin: 0 auto;
  display: grid;
  grid-gap: 1rem;
`;

const InputLabel = styled.label`
  font-family: ${({ theme }) => theme.font.body};
  font-style: italic;
  color: ${({ theme }) => theme.color.brandPurpleDarker};
`;

const InputField = styled.input`
  border: 2px solid ${({ theme }) => theme.color.brandPurple};
  font-size: ${({ theme }) => theme.modularScale.medium};
  padding: ${({ theme }) => theme.modularScale.medium};
  ::placeholder: {
    color: ${({ theme }) => theme.color.brandPurple};
  }
`;

const RegisterButton = styled.button`
  margin: 0 auto 4rem auto;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-style: italic;
  text-decoration: none;
  color: ${({ theme }) => theme.color.white};
  cursor: pointer;
  background: ${({ theme }) => theme.color.brandPurpleDarker};
  padding: ${({ theme}) => theme.modularScale.base} ${({ theme}) => theme.modularScale.xlarge};
`;

const RegisterForm = ({ jwt, username }) => (
  <Formik
    initialValues={{
      username: '',
      email: '',
      password: ''
    }}

    initialStatus={{ button: 'Register' }}

    validationSchema={
      Yup.object().shape({
        username: Yup.string().required('Please choose a username'),
        email: Yup.string().email('Please enter a valid Email Address').required('Email is required'),
        password: Yup.string().required('Password is required')
      })
    }

    onSubmit={(values, actions) => {
      actions.setStatus({ button: 'Registering...' });

      register(values)
        .then(async (res) => {
          if (res.error) {
            console.error(res);
            return;
          }
          actions.setSubmitting(false);
          actions.setStatus({ button: 'Registered Successfully!' });
        })
        .catch((e) => {
          actions.setSubmitting(false);
          actions.setStatus({ button: 'Register' })
        })
    }}

    validateOnChange={true}
  >
    {({ status, errors, values, isSubmitting, handleSubmit, handleChange, }) => {
      return (
        <Form onSubmit={handleSubmit}>
          <InputLabel htmlFor="register-username">
            Username
          </InputLabel>
          <InputField
            name="username"
            id="register-username"
            placeholder="CupcakeLover"
            type="text"
            onChange={handleChange}
            value={values.username}
          />
          {errors.username && <span>{errors.username}</span>}

          <InputLabel htmlFor="register-email">
            Email
          </InputLabel>
          <InputField
            name="email"
            id="register-email"
            placeholder="email@example.com"
            type="email"
            onChange={handleChange}
            value={values.email}
          />
          {errors.email && <span>{errors.email}</span>}

          <InputLabel htmlFor="register-password">
            Password
          </InputLabel>
          <InputField
            name="password"
            id="register-password"
            placeholder="••••••••"
            type="password"
            onChange={handleChange}
            value={values.password}
          />
          {errors.password && <span>{errors.password}</span>}

          <RegisterButton type="submit" disabled={isSubmitting}>{status.button}</RegisterButton>
        </Form>
      )
    }
    }
  </Formik>
);

RegisterForm.getInitialProps = async (ctx) => {
  const { jwt = '', username = '', } = cookies(ctx);
  return { jwt, username };
};

export default RegisterForm;

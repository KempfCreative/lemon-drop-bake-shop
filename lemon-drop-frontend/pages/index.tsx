import React, {
  useState,
  useEffect
} from 'react';
import styled from 'styled-components';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import { getPageWithParams } from '../api/cms/pages';
import Header from '../ui/header';
import Account from '../ui/account';
import Cart from '../ui/cart';
import { generateMeta } from '../utils/generateMeta';

// TODO implement typescript
// interface Props {
//   page?: object;
// }

const UserWrapper = styled.div`
  position: fixed;
  bottom: 0;
  display: flex;
  justify-content: space-between;
  width: 100vw;
  margin-left: -8.5vw;
  background: ${({ theme }) => theme.color.white};
  padding-top: 1rem;
  z-index: ${({ theme }) => theme.layers.audience};
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    position: static;
    bottom: unset;
    justify-content: unset;
    width: unset;
    margin-left: 0;
    background: transparent;
    padding-top: 0;
    z-index: unset;
  }
`;

const Loading = styled.div`
  background-color: black;
  bottom: 0;
  left: 0;
  opacity: 0.75;
  pointer-events: none;
  position: fixed;
  right: 0;
  top: 0;
  visibility: visible;

  ::after {
    animation: spin .5s infinite linear;
    border: 0.25rem solid black;
    border-radius: 290486px;
    border-right-color: transparent;
    border-top-color: transparent;
    content: '';
    height: 2rem;
    left: calc(50% - (2rem / 2));
    position: absolute;
    top: calc(50% - (2rem / 2));
    width: 2rem;

    @keyframes spin {
      from { transform: rotate(0) }
      to { transform: rotate(359deg) }
    }
  }
`;

// TODO implement the NextPage interface
const Index = ({ page }) => {
  const [content, setContent] = useState([]);

  useEffect(() => {
    if (page.Content) {
      const components = page.Content.map((content) => {
        const componentPath = content.__component.split('.').join('/');
        return (
          {
            Component: dynamic(
              () => import(`../${componentPath}/index.tsx`),
              {
                loading: () => <Loading />
              }
            ),
            data: content
          }
        );
      });
      setContent(components);  
    }
  }, []);

  return (
    <>
      <Head>
        <title>{page?.title ?? 'Test'}</title>
        { generateMeta() }
        { page?.metatag.length > 0 
          && (
            <>
              {
                page.metatag.map((meta, index) => (
                  <meta
                    key={`${meta.name}-${index}`}
                    name={meta.name}
                    content={meta.value}
                  />
                ))
              }
            </>
          )
        }
      </Head>
      { content.length > 0
        && (
          <>
            {
              content.map((contentLoaded, index) => {
                const {
                  Component,
                  data
                } = contentLoaded;
                return <Component key={`${data.id}-${index}`} data={data} />;
              })
            }
          </>
        )
      }
    </>
  );
 };

Index.getInitialProps = async (ctx) => {
  let page;
  const pages = await getPageWithParams({ name: 'uri', value: '/index' });
  page = pages?.length > 1 ? pages[0] : pages;
  return { page };
}

export default Index;

import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import cookies from 'next-cookies';
import { getCategoryWithParams } from '../../api/cms/category';
import Header from '../../ui/header';
import Account from '../../ui/account';
import ProductCard from '../../ui/product-card';
import Typography from '../../ui/typography';
import PLPMeta from '../../utils/generateMeta';

const CategoryHeader = styled.header`
  background: ${({ theme }) => theme.color.brandPurple};
  padding: ${({ theme }) => theme.modularScale.xlarge} ${({ theme }) => theme.modularScale.medium};
  margin: ${({ theme }) => theme.modularScale.medium} 0;
`;

const CategoryHeading = styled.h1`
  text-align: center;
  font-family: ${({ theme }) => theme.font.head};
  font-size: ${({ theme }) => theme.modularScale.large};
  color: ${({ theme }) => theme.color.white};
  font-weight: bold;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    padding: 4rem 0;
    font-size: ${({ theme }) => theme.modularScale.xlarge};
  }
`;

const CategoryImage = styled.img`
`;

const ProductGrid = styled.section`
  width: 90%;
  margin: ${({ theme }) => theme.modularScale.medium} auto;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 4rem;
  @media screen and (min-width: ${({ theme }) => theme.breakpoint.small}) {
    grid-template-columns: 1fr 1fr 1fr 1fr;
  }
`;

// Cakes Styles:

const CakesAside = styled.aside`
  background-color: ${({ theme }) => theme.color.brandPurple};
  margin: ${({ theme }) => theme.modularScale.large};
  padding: ${({ theme }) => theme.modularScale.large};
  color: ${({ theme }) => theme.color.white};
`;

const AsideContent = styled(Typography)`
  color: ${({ theme }) => theme.color.white};
`;

const CustomCakeTable = styled.table`
  margin: 0 auto;
  tr {
    border: 1px solid ${({ theme }) => theme.color.brandPurple};
  }
  tr > * {
    padding: ${({ theme }) => theme.modularScale.medium};
    color: ${({ theme }) => theme.color.brandPurpleDarker};
  }
  tr > th {
    background-color: ${({ theme }) => theme.color.brandPurple};
    color: ${({ theme }) => theme.color.white};
  }
`;

const FlavorsList = styled.ul`
  margin: 0 auto;
  padding: 0 0 2rem 0;
  > li {
    text-align: center;
    color: ${({ theme }) => theme.color.brandPurpleDarker};
    padding: 0 0 ${({ theme }) => theme.modularScale.small} 0;
  }
`;

const ShopType = ({ category, username, orderHash }) => {
  const [products, setProducts] = useState([]);
  
  useEffect(() => {
    const activeProducts = category.products.filter((p) => p.Active);
    setProducts(activeProducts);
  }, [category.taxonName]);

  return (
    <>
      <PLPMeta
        headers={{ header: `${category?.taxonName ?? 'Baked Good'}s` }}
        results={products}
      />
      { category?.taxonName ? (
        <CategoryHeader>
          <CategoryHeading>{category.taxonName}s</CategoryHeading>
          { category?.icon && <CategoryImage src={category.icon.uri} /> }
        </CategoryHeader>
      ) : null }
      { category.taxonName === 'Cake' ? (
        <>
          <CakesAside>
            <AsideContent data={{ type: "paragraph", value: "All Cakes are Pickup Only. This is to ensure that you get your dessert in the absolute best condition."}} />
          </CakesAside>
          <CakesAside>
            <AsideContent data={{ type: "paragraph", value: "Custom cake orders must contact Suzanne at <a href='mailto:hellolemondropbakeshop@gmailcom'>HelloLemonDropBakeShop</a> or via phone at (716)523-6160"}} />
          </CakesAside>
        </>
        ) : null
      }
      {products.length > 0 && (
        <ProductGrid>
          {products.map((p) => (
            <ProductCard 
              product={p}
              orderHash={orderHash}
              username={username}
              key={p.slug}
              single={category.taxonName === 'Cake'}
            />
          ))}
        </ProductGrid>
      )}
      {
        category.taxonName === 'Cake' ? (
          <>
            <section>
              <Typography data={{ type: "heading", value: "Custom Round" }} />
              <CustomCakeTable>
                <tr>
                  <th>Size</th>
                  <th>Serving</th>
                  <th>Price</th>
                </tr>
                <tr>
                  <td>6"</td>
                  <td>Serves 8</td>
                  <td>Starting At $20.00</td>
                </tr>
                <tr>
                  <td>8"</td>
                  <td>Serves 14</td>
                  <td>Starting At $24.00</td>
                </tr>
                <tr>
                  <td>9"</td>
                  <td>Serves 18</td>
                  <td>Starting At $28.00</td>
                </tr>
                <tr>
                  <td>10"</td>
                  <td>Serves 21</td>
                  <td>Starting At $32.00</td>
                </tr>
                <tr>
                  <td>12"</td>
                  <td>Serves 28</td>
                  <td>Starting At $40.00</td>
                </tr>
                <tr>
                  <td>14"</td>
                  <td>Serves 41</td>
                  <td>Starting At $48.00</td>
                </tr>
              </CustomCakeTable>
            </section>
            <section>
              <Typography data={{ type: "heading", value: "Custom Sheet" }} />
              <CustomCakeTable>
                <tr>
                  <th>Size Name</th>
                  <th>Size</th>
                  <th>Serving</th>
                  <th>Price</th>
                </tr>
                <tr>
                  <td>Quarter Sheet</td>
                  <td>9" X 13"</td>
                  <td>Serves 18</td>
                  <td>Starting at $28.00</td>
                </tr>
                <tr>
                  <td>Half Sheet</td>
                  <td>12" X 18"</td>
                  <td>Serves 36</td>
                  <td>Starting at $45.00</td>
                </tr>
                <tr>
                  <td>Full Sheet</td>
                  <td>16" X 24"</td>
                  <td>Serves 64</td>
                  <td>Starting at $75.00</td>
                </tr>
              </CustomCakeTable>
            </section>
            <section>
              <Typography data={{ type: "heading", value: "Cake Flavors" }} />
              <FlavorsList>
                <li>Vanilla</li>
                <li>Chocolate</li>
                <li>Red Velvet</li>
                <li>Spice</li>
                <li>Funfetti</li>
                <li>Banana</li>
                <li>Carrot</li>
                <li>Strawberry</li>
                <li>Mint Chocolate</li>
              </FlavorsList>
            </section>
            <section>
              <Typography data={{ type: "heading", value: "Frosting Flavors" }} />
              <FlavorsList>
                <li>Vanilla Buttercream</li>
                <li>Chocolate Buttercream</li>
                <li>Brown Sugar Buttercream</li>
                <li>Swiss Meringue Buttercream</li>
                <li>Cream Cheese Frosting</li>
                <li>Chocolate Cream Cheese Frosting</li>
              </FlavorsList>
            </section>
          </>
        ) : null
      }
    </>
  );
};

ShopType.getInitialProps = async (ctx) => {
  const { type } = ctx.query;
  const category = await getCategoryWithParams({ name: 'uri', value: type });
  const { username = '', orderHash = '' } = cookies(ctx);
  return { category: category[0], username, orderHash };
}

export default ShopType;

import { useState, useEffect } from 'react';
import styled from 'styled-components';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import cookies from 'next-cookies';
import Header from '../../ui/header';
import Account from '../../ui/account';
import Payment from '../../ui/payment';
import Typography from '../../ui/typography';

const CheckoutPage = ({ username, step, orderDbId, orderHash }) => {
  const [stepHeading, setStepHeading] = useState('');

  const StepTemplate = {
    StepName: dynamic(() => import(`../../ui/${step}/index.tsx`)), username: username, orderDbId: orderDbId, orderHash: orderHash
  };
  
  useEffect(() => {
    const checkoutStep = ['cart', 'address', 'payment', 'confirm'].indexOf(step);
    const checkoutLength = ['cart', 'address', 'payment', 'confirm'].length;
    const stepString = () => {
      if (typeof step==undefined) return;
      const firstLetter = step[0] || step.charAt(0);
      return firstLetter ? step.replace(/^./, firstLetter.toUpperCase()) : '';
    }
    const checkoutHeading = `Secure Checkout - Step ${checkoutStep} of ${checkoutLength - 1} - ${stepString()}`;
    setStepHeading(checkoutHeading);
  }, [step]);

  return (
    <>
      <Typography data={{ type: "heading", value: `${stepHeading}` }} />
      <StepTemplate.StepName username={StepTemplate.username} orderDbId={StepTemplate.orderDbId} orderHash={StepTemplate.orderHash} />
    </>
  );
};

CheckoutPage.getInitialProps = async (ctx) => {
  const { step } = ctx.query;
  const { username = '', orderDbId = 0, orderHash = '' } = cookies(ctx);
  return { username,  step, orderDbId, orderHash };
}

export default CheckoutPage;

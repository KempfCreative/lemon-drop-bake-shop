import { useState, useEffect } from 'react';
import styled from 'styled-components';
import Head from 'next/head';
import cookies from 'next-cookies';
import Header from '../../ui/header';
import Account from '../../ui/account';
import Payment from '../../ui/payment';
import Typography from '../../ui/typography';
import { generateMeta } from '../../utils/generateMeta';

const CheckoutPage = ({ username, orderDbId, orderHash }) => (
  <>
    <Head>
      {generateMeta()}
    </Head>
    <>
      <Typography data={{ type: "heading", value: "Checkout" }} />
      <Payment username={username} orderDbId={orderDbId} orderHash={orderHash} />
    </>
  </>
);

CheckoutPage.getInitialProps = async (ctx) => {
  const { username = '', orderDbId = 0, orderHash = '' } = cookies(ctx);
  return { username, orderDbId, orderHash };
}

export default CheckoutPage;

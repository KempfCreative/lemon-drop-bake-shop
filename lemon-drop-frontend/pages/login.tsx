import React from 'react';
import * as Yup from 'yup';
import Router from 'next/router';
import Link from 'next/link';
import styled from 'styled-components';
import { Formik } from 'formik';
import cookies from 'next-cookies';
import { login } from '../api/cms/auth';
import Header from '../ui/header';
import Account from '../ui/account';
import Cart from '../ui/cart';

const globalAny:any = global;

const Form = styled.form`
  width: 90%;
  margin: 0 auto;
  display: grid;
  grid-gap: 1rem;
`;

const InputLabel = styled.label`
  font-family: ${({ theme }) => theme.font.body};
  font-style: italic;
  color: ${({ theme }) => theme.color.brandPurpleDarker};
`;

const InputField = styled.input`
  border: 2px solid ${({ theme }) => theme.color.brandPurple};
  font-size: ${({ theme }) => theme.modularScale.medium};
  padding: ${({ theme }) => theme.modularScale.medium};
  ::placeholder: {
    color: ${({ theme }) => theme.color.brandPurple};
  }
`;

const Buttons = styled.div`
  display: flex;
  justify-content: center;
  margin: 0 auto 4rem;
`;

const LoginButton = styled.button`
  margin: 0 auto 4rem auto;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-style: italic;
  text-decoration: none;
  color: ${({ theme }) => theme.color.white};
  cursor: pointer;
  background: ${({ theme }) => theme.color.brandPurpleDarker};
  padding: ${({ theme}) => theme.modularScale.base} ${({ theme}) => theme.modularScale.xlarge};
`;

const RegisterLink = styled.a`
  margin: 0 1rem 4rem;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-style: italic;
  text-decoration: none;
  color: ${({ theme }) => theme.color.white};
  cursor: pointer;
  background: ${({ theme }) => theme.color.brandPurpleDarker};
  padding: ${({ theme}) => theme.modularScale.base} ${({ theme}) => theme.modularScale.xlarge};
`;

const PasswordReset = styled.a`
  margin: 0 1rem 4rem;
  display: block;
  width: 100%;
  font-family: ${({ theme }) => theme.font.body};
  font-size: ${({ theme }) => theme.modularScale.medium};
  font-style: italic;
  text-decoration: none;
  text-align: center;
  color: ${({ theme }) => theme.color.brandPurpleDarker};
`;

const LoginForm = ({ jwt, username, orderDbId, orderHash }) => (
  <Formik
    initialValues={{
      identifier: '',
      password: ''
    }}

    initialStatus={{ button: 'Login' }}

    validationSchema={
      Yup.object().shape({
        identifier: Yup.string().required('Please tell us a username or email'),
        password: Yup.string().required('Password is required')
      })
    }

    onSubmit={(values, actions) => {
      actions.setStatus({ button: 'Logging In...' });

      login(values)
        .then(async (res) => {
          if (res.error) {
            actions.setSubmitting(false);
            actions.setStatus({ button: 'Login' })
            return;
          }
          actions.setSubmitting(false);
          globalAny.document.cookie = `jwt=${res.jwt}; path=/`;
          globalAny.document.cookie = `username=${res.user.username}; path=/`;
          globalAny.document.cookie = `email=${res.user.email}; path=/`;
          globalAny.document.cookie = `orders=${JSON.stringify(res.user.orders)}; path=/`;
          globalAny.document.cookie = `orderDbId=${orderDbId}; path=/`;
          globalAny.document.cookie = `orderHash=${orderHash}; path=/`;
          actions.setStatus({ button: 'Logged In' });
        })
        .catch((e) => {
          actions.setSubmitting(false);
          actions.setStatus({ button: 'Login' })
        })
    }}

    validateOnChange={true}
  >
    {({ status, errors, values, isSubmitting, handleSubmit, handleChange, }) => {
      return (
        <>
          <Form onSubmit={handleSubmit}>
            <InputLabel htmlFor="login-identifier">
              Email/Username
            </InputLabel>
            <InputField
              name="identifier"
              id="login-identifier"
              placeholder="CupcakeLover OR cupcakelover@email.com"
              type="text"
              onChange={handleChange}
              value={values.identifier}
            />
            {errors.identifier && <span>{errors.identifier}</span>}

            <InputLabel htmlFor="login-password">
              Password
            </InputLabel>
            <InputField
              name="password"
              id="login-password"
              placeholder="••••••••"
              type="password"
              onChange={handleChange}
              value={values.password}
            />
            {errors.password && <span>{errors.password}</span>}
            <Buttons>
              <LoginButton type="submit" disabled={isSubmitting}>{status.button}</LoginButton>
              <Link href="/register" passHref>
                <RegisterLink href="/register">Register</RegisterLink>
              </Link>
            </Buttons>
          </Form>
          <Link href="/password/recover" passHref>
            <PasswordReset>Forgot your password?</PasswordReset>
          </Link>
        </>
      )
    }
    }
  </Formik>
);

LoginForm.getInitialProps = async (ctx) => {
  const { jwt = '', username = '', orderDbId = 0, orderHash = '' } = cookies(ctx);
  return { jwt, username, orderDbId, orderHash };
}

export default LoginForm;

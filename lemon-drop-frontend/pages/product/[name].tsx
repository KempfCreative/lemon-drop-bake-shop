import styled from 'styled-components';
import Head from 'next/head';
import cookies from 'next-cookies';
import { getProductWithParams } from '../../api/cms/product';
import Header from '../../ui/header';
import Account from '../../ui/account';
import ProductCard from '../../ui/product-card';
import { generateMeta } from '../../utils/generateMeta';

const globalAny:any = global;

const ProductName = ({ product, jwt, username, orderHash  }) => (
  <>
    <Head>
      {
        generateMeta({
          key: 'product',
          title: `${product?.Title} - ${product?.category?.taxonName} - Lemon Drop Bake Shop` ?? 'Lemon Drop Bake Shop',
          description: `${product?.Description} - ${product?.category?.taxonName} - Lemon Drop Bake Shop`,
          og: {
            description: `${product?.Description} - ${product?.category?.taxonName} - Lemon Drop Bake Shop`,
            image: product?.Asset?.length > 0
              ? product.Asset?.[0]?.url ?? null
              : null,
            title: `${product?.Title} - ${product?.category?.taxonName} - Lemon Drop Bake Shop` ?? 'Lemon Drop Bake Shop',
            url: process?.browser ?? globalAny.window.location.href
          },
          twitter: {
            description: `${product?.Description} - ${product?.category?.taxonName} - Lemon Drop Bake Shop`,
            image: product?.Asset?.length > 0
              ? product.Asset?.[0]?.url ?? null
              : null,
            title: `${product?.Title} - ${product?.category?.taxonName} - Lemon Drop Bake Shop` ?? 'Lemon Drop Bake Shop'
          }
        })
      }
    </Head>
    <ProductCard single={product?.category?.taxonName === 'Cake'} product={product} orderHash={orderHash} username={username} />
  </>
);

ProductName.getInitialProps = async (ctx) => {
  const { name } = ctx.query;
  const products = await getProductWithParams({ name: 'slug', value: name });
  const product = {...products[0], full: true};
  const { username = '', orderHash = '' } = cookies(ctx);
  return { product, username, orderHash };
}

ProductName.whyDidYouRender = true;

export default ProductName;

import uuidv4 from 'uuid/v4';
import { newOrder, putOrder, getOrderWithParams } from '../api/cms/order';


const crudOrder = async (orderId, productData) => {
  if (orderId) {
    const { id } = await getOrderWithParams({name: "orderId", value: orderId}).then(order => order.length > 0 ? order[0] : {id: 0});
    const updatedOrder = await putOrder({ id, body: productData });
    return updatedOrder;
  }
  const orderUuid = uuidv4();
  // TODO figure out how to put usernames associated with this....im so tired
  const newOrderData = await newOrder({ body: { orderId: orderUuid, ...productData }});
  return newOrderData;
};

export default crudOrder;

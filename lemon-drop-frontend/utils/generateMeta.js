import React, { Fragment, createElement } from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';

const DEFAULTS = {
  description: 'Suzanne Kempf started this WNY / Buffalo Bakery in 2020. This scratch and made to order bakery is available for pickup, delivery and mail order goods.',
  image: 'https://lemon-drop-bake-shop.s3.amazonaws.com/facebook-cover.jpg',
  title: 'Lemon Drop Bake Shop',
  keywords: 'Bakery, Cupcakes, Cookies, Cakes, Niagara Falls Bakery, Buffalo Bakery, Wheatfield Bakery, Bake Shop'
};

export const meta = {
  title: DEFAULTS.title,
  description: DEFAULTS.description,
  keywords: DEFAULTS.keywords,
  og: {
    site_name: 'Lemon Drop Bake Shop',
    description: DEFAULTS.description,
    image: DEFAULTS.image,
    title: DEFAULTS.title,
    type: 'website',
    url: process.env.NEXT_PUBLIC_CLIENT_HOST
  },
  twitter: {
    url: process.env.NEXT_PUBLIC_CLIENT_HOST,
    card: 'summary_large_image',
    description: DEFAULTS.description,
    image: DEFAULTS.image,
    title: DEFAULTS.title
  }
};

export const generateMeta = (o = {}) => {
  const options = { ...meta, key: 'default', ...o };
  const children = [];

  /* HTML Meta Tags */
  children.push(createElement('title', { key: 'title' }, options.title));
  children.push(createElement('meta', { key: 'keywords', name: 'keywords', content: options.keywords }));
  children.push(createElement('meta', { key: 'description', name: 'description', content: options.description }));

  /* Facebook Meta Tags */
  Object.entries(options.og).forEach(([key, value]) => {
    if (value != null) children.push(createElement('meta', { key: `og-${key}`, property: `og:${key}`, content: value }));
    else children.push(createElement('meta', { key: `og-${key}`, property: `og:${key}`, content: meta.og[key] }));
  });

  /* Twitter Meta Tags */
  Object.entries(options.twitter).forEach(([key, value]) => {
    if (value != null) children.push(createElement('meta', { key: `twitter-${key}`, name: `twitter:${key}`, content: value }));
    else children.push(createElement('meta', { key: `twitter-${key}`, name: `twitter:${key}`, content: meta.twitter[key] }));
  });

  return createElement(Fragment, {}, [...children]);
};

export const getDescription = (key, values, mutator) => {
  const MAX_LENGTH = 158;
  const BreakException = {};

  if (values) {
    try {
      values.forEach((c, i) => {
        if (i === 0) mutator.push(`including ${c[key]}`);
        else if (i === values.length - 1) mutator.push(`and ${c[key]}`);
        else {
          const temp = mutator.slice(0);
          temp.push(c[key]);
          temp.push(` and ${values[i + 1][key]}`);

          if (`${temp.join(', ')}.`.length >= MAX_LENGTH) {
            mutator.push(`and ${c[key]}`);
            throw BreakException;
          } else {
            mutator.push(c[key]);
          }
        }
      });
    } catch (e) {
      if (e !== BreakException) throw e;
    }
  }

  return `${mutator.join(', ')}.`;
};

export const getPLPDescription = (activeCategories = [], facets = false, products = false) => {
  const description = [activeCategories.map((c) => c.name).join(' ')];

  if (!facets || !products) return DEFAULTS.description;

  if (activeCategories.length <= 0 || activeCategories.length === 3) {
    return getDescription('title', products, description);
  }

  const categories = facets.filter(
    (f) => /^cat[1-9]+$/.test(f.id) && +f.id.split('cat')[1] > activeCategories.length
  ).reduce((acc, curr) => [...acc, curr.values], [])[0];

  return getDescription('name', categories, description);
};

const PLPMeta = (props) => {
  const getMetaData = () => {
    return generateMeta({
      key: 'shop',
      title: `Shop ${props?.headers?.header ?? ''}${props.headers?.subHeaders?.map((h) => `, ${h?.name}`).join('') ?? ''} - Lemon Drop Bake Shop`,
      description: getPLPDescription(
        Object.values(props.categories?.active ?? []), props?.facets, props?.results
      ),
      og: {
        title: `Shop ${props?.headers?.header ?? ''}${props.headers?.subHeaders?.map((h) => `, ${h?.name}`).join('') ?? ''} - Lemon Drop Bake Shop`,
        image: props.results?.[0]?.Asset?.[0]?.url ?? DEFAULTS.image,
        url: process?.browser ?? global.window.location.href,
        description: getPLPDescription(
          Object.values(props.categories?.active ?? []), props?.facets, props?.results
        )
      },
      twitter: {
        title: `Shop ${props?.headers?.header ?? ''}${props.headers?.subHeaders?.map((h) => `, ${h?.name}`).join('') ?? ''} - Lemon Drop Bake Shop`,
        image: props.results?.[0]?.Asset?.[0]?.url ?? DEFAULTS.image,
        description: getPLPDescription(
          Object.values(props.categories?.active ?? []), props?.facets, props?.results
        )
      }
    });
  };

  return <Head>{getMetaData(props)}</Head>;
};

PLPMeta.defaultProps = {
  brand: null,
  categories: null,
  correctedSearchTerm: null,
  edit: null,
  headers: null,
  lp: null,
  lpc: null,
  noResults: false,
  results: null,
  searchTerm: '*',
  trend: null,
  trendName: null
};

PLPMeta.propTypes = {
  brand: PropTypes.object,
  categories: PropTypes.object,
  correctedSearchTerm: PropTypes.string,
  edit: PropTypes.string,
  headers: PropTypes.object,
  lp: PropTypes.object,
  lpc: PropTypes.string,
  noResults: PropTypes.bool,
  results: PropTypes.array,
  searchTerm: PropTypes.string,
  trend: PropTypes.string,
  trendName: PropTypes.string
};

export default PLPMeta;

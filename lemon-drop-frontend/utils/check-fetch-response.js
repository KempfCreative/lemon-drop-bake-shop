const checkFetchResponse = (response) => {
  if (response.ok) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = {
    error: response.statusText,
    status: response.status
  };

  return Promise.reject(error);
};

module.exports = checkFetchResponse;

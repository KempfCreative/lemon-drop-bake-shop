/* eslint-disable no-undef, no-unused-expressions */

typeof window.IntersectionObserver === 'undefined'
  ? require('intersection-observer')
  : '';

typeof Promise === 'undefined' || Promise.prototype.finally === undefined
  ? require('promise-polyfill/src/polyfill')
  : '';

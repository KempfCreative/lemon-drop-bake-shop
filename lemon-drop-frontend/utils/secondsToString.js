export const secondsToDays = (timestamp) => {
  const timeNum = Number.isInteger(timestamp)
    ? timestamp
    : parseInt(timestamp, 10);
  const days = timeNum/(60*60*24);
  return `Ready in ~${days} Days.`
}
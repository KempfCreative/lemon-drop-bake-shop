# Next application

A quick description of your next application

## Docker

```
docker build \
    --build-arg NODE_ENV=production \
    --build-arg NEXT_PUBLIC_ASSET_HOST=x \
    --build-arg NEXT_PUBLIC_CLIENT_HOST=http://localhost:3000 \
    --build-arg NEXT_PUBLIC_CMS_HOST=https://strapi.lemondropbake.shop \
    --build-arg NEXT_PUBLIC_SQUARE_SANDBOX_APP_ID=x \
    --build-arg SQUARE_SANDBOX_ACCESS_TOKEN=x \
    --build-arg NEXT_PUBLIC_SQUARE_APP_ID=x \
    --build-arg SQUARE_ACCESS_TOKEN=x \
    -t lemon-drop-frontend .
```

```
docker run -p 3000:3000 lemon-drop-frontend:latest
```

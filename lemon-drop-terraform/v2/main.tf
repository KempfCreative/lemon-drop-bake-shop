provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "ld-prd-tfstate"
    key    = "lemon-drop-production.tfstate"
    region = "us-east-1"
  }
}

module "lemon_drop_frontend_application" {
  source      = "git::https://github.com/cloudposse/terraform-aws-elastic-beanstalk-application.git?ref=tags/0.3.0"
  namespace   = "lemon-drop"
  stage       = "production"
  name        = "frontend"
  description = "Lemon Drop Frontend Application"
}

module "lemon_drop_frontend_environment" {
  source                             = "git::https://github.com/cloudposse/terraform-aws-elastic-beanstalk-environment.git?ref=tags/0.18.0"
  name                               = "frontend"
  description                        = "Lemon Drop Frontend Production"
  namespace                          = "lemon-drop"
  stage                              = "production"
  region                             = "us-east-1"
  dns_zone_id                        = "Z2TVQEPNXLN7ZS"
  dns_subdomain                      = "www"
  elastic_beanstalk_application_name = module.lemon_drop_frontend_application.elastic_beanstalk_application_name


  availability_zone_selector   = "Any 2"
  wait_for_ready_timeout       = "10m"
  environment_type             = "LoadBalanced"
  loadbalancer_type            = "application"
  loadbalancer_certificate_arn = "arn:aws:acm:us-east-1:712489879307:certificate/588350d9-a0b9-4fbd-a90a-a65c9325cdce"
  loadbalancer_ssl_policy      = "ELBSecurityPolicy-2016-08"
  elb_scheme                   = "public"
  tier                         = "WebServer"
  force_destroy                = true
  associate_public_ip_address  = true

  instance_type    = "t3.micro"
  root_volume_size = 8
  root_volume_type = "gp2"

  autoscale_min             = 1
  autoscale_max             = 2
  autoscale_measure_name    = "CPUUtilization"
  autoscale_statistic       = "Average"
  autoscale_unit            = "Percent"
  autoscale_lower_bound     = 20
  autoscale_lower_increment = -1
  autoscale_upper_bound     = 80
  autoscale_upper_increment = 1

  vpc_id                       = "vpc-898eb9f3"
  loadbalancer_subnets         = ["subnet-96a4bff1", "subnet-e43adbc5"]
  application_subnets          = ["subnet-96a4bff1", "subnet-e43adbc5"]
  allowed_security_groups      = ["sg-361ba61d"]
  loadbalancer_security_groups = ["sg-361ba61d"]

  rolling_update_enabled  = true
  rolling_update_type     = "Health"
  updating_min_in_service = 0
  updating_max_batch      = 1

  healthcheck_url  = "/"
  application_port = 80

  solution_stack_name = "64bit Amazon Linux 2018.03 v2.19.0 running Multi-container Docker 18.09.9-ce (Generic)"

  additional_settings = [
    {
      namespace = "aws:elasticbeanstalk:environment:process:default"
      name      = "StickinessEnabled"
      value     = "true"
    },
    {
      namespace = "aws:elasticbeanstalk:managedactions"
      name      = "ManagedActionsEnabled"
      value     = "true"
    }
  ]
}

module "lemon_drop_backend_application" {
  source      = "git::https://github.com/cloudposse/terraform-aws-elastic-beanstalk-application.git?ref=tags/0.3.0"
  namespace   = "lemon-drop"
  stage       = "production"
  name        = "backend"
  description = "Lemon Drop Backend Application"
}

module "lemon_drop_backend_environment" {
  source                             = "git::https://github.com/cloudposse/terraform-aws-elastic-beanstalk-environment.git?ref=tags/0.18.0"
  name                               = "backend"
  description                        = "Lemon Drop Backend Production"
  namespace                          = "lemon-drop"
  stage                              = "production"
  region                             = "us-east-1"
  dns_zone_id                        = "Z2TVQEPNXLN7ZS"
  dns_subdomain                      = "strapi"
  elastic_beanstalk_application_name = module.lemon_drop_backend_application.elastic_beanstalk_application_name


  availability_zone_selector   = "Any 2"
  wait_for_ready_timeout       = "10m"
  environment_type             = "LoadBalanced"
  loadbalancer_type            = "application"
  loadbalancer_certificate_arn = "arn:aws:acm:us-east-1:712489879307:certificate/588350d9-a0b9-4fbd-a90a-a65c9325cdce"
  loadbalancer_ssl_policy      = "ELBSecurityPolicy-2016-08"
  elb_scheme                   = "public"
  tier                         = "WebServer"
  force_destroy                = true
  associate_public_ip_address  = true

  instance_type    = "t3.micro"
  root_volume_size = 8
  root_volume_type = "gp2"

  autoscale_min             = 1
  autoscale_max             = 2
  autoscale_measure_name    = "CPUUtilization"
  autoscale_statistic       = "Average"
  autoscale_unit            = "Percent"
  autoscale_lower_bound     = 20
  autoscale_lower_increment = -1
  autoscale_upper_bound     = 80
  autoscale_upper_increment = 1

  vpc_id                       = "vpc-898eb9f3"
  loadbalancer_subnets         = ["subnet-96a4bff1", "subnet-e43adbc5"]
  application_subnets          = ["subnet-96a4bff1", "subnet-e43adbc5"]
  allowed_security_groups      = ["sg-361ba61d"]
  loadbalancer_security_groups = ["sg-361ba61d"]

  rolling_update_enabled  = true
  rolling_update_type     = "Health"
  updating_min_in_service = 0
  updating_max_batch      = 1

  healthcheck_url  = "/"
  application_port = 80

  solution_stack_name = "64bit Amazon Linux 2018.03 v2.19.0 running Multi-container Docker 18.09.9-ce (Generic)"

  additional_settings = [
    {
      namespace = "aws:elasticbeanstalk:environment:process:default"
      name      = "StickinessEnabled"
      value     = "true"
    },
    {
      namespace = "aws:elasticbeanstalk:managedactions"
      name      = "ManagedActionsEnabled"
      value     = "true"
    }
  ]
}

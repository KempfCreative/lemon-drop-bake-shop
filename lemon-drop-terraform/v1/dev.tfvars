region = "us-east-1"

availability_zones = ["us-east-1a", "us-east-1b"]

namespace = "ldb"

stage = "prd"

name = "lemon-drop"

description = "Lemon Drop Bake Shop"

tier = "WebServer"

environment_type = "LoadBalanced"

loadbalancer_type = "application"

loadbalancer_certificate_arn = "arn:aws:acm:us-east-1:712489879307:certificate/588350d9-a0b9-4fbd-a90a-a65c9325cdce"

availability_zone_selector = "Any 2"

instance_type = "t3.micro"

autoscale_min = 1

autoscale_max = 2

wait_for_ready_timeout = "20m"

force_destroy = true

rolling_update_enabled = true

rolling_update_type = "Health"

updating_min_in_service = 0

updating_max_batch = 1

healthcheck_url = "/"

application_port = 80

root_volume_size = 8

root_volume_type = "gp2"

autoscale_measure_name = "CPUUtilization"

autoscale_statistic = "Average"

autoscale_unit = "Percent"

autoscale_lower_bound = 20

autoscale_lower_increment = -1

autoscale_upper_bound = 80

autoscale_upper_increment = 1

elb_scheme = "public"

solution_stack_name = "64bit Amazon Linux 2018.03 v2.19.0 running Multi-container Docker 18.09.9-ce (Generic)"

version_label = ""

dns_zone_id = "Z2TVQEPNXLN7ZS"

additional_settings = [
  {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "StickinessEnabled"
    value     = "true"
  },
  {
    namespace = "aws:elasticbeanstalk:managedactions"
    name      = "ManagedActionsEnabled"
    value     = "true"
  }
]

env_vars = {
    "APP" = "lemon-drop"
}
'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async create(ctx) {
    let entity;
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.order.create(data, { files });
    } else {
      entity = await strapi.services.order.create(ctx.request.body);
    }
    return sanitizeEntity(entity, { model: strapi.models.order });
  },
  async update(ctx) {
    let entity;
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.order.update(ctx.params, data, {
        files,
      });
    } else {
      entity = await strapi.services.order.update(
        ctx.params,
        ctx.request.body
      );
    }

    const entry = sanitizeEntity(entity, { model: strapi.models.order });
    
    if (entry.status === 'confirm') {

      // send an email by using the email plugin
      await strapi.plugins['email'].services.email.send({
        to: 'hellolemondropbakeshop@gmail.com',
        from: 'admin@lemondropbake.shop',
        subject: `LemonDropBakeShop New Order #${entry.id} for ${entry.customerName}`,
        text: `
          Products to create:\n\n
          ${ entry.cart.length > 0
            && entry.cart.map((cartItem) => (
              `Item: ${cartItem.Title}\n\n
               Quantity: ${cartItem.quantity}\n\n`))
          }
          ---\n\n
          Customer Information:\n\n
          Name: ${entry.customerName}\n\n
          Phone: ${entry.phoneNumber}\n\n
          Email: ${entry.email}\n\n
          Address: ${entry.addressStreet}, ${entry.addressCity} ${entry.addressState}\n\n
          ---\n\n
          ${entry.specialInstructions !== undefined
            && `Special Instructions:\n\n ${entry.specialInstructions}`
          }
        `,
        html: `
          <h2>Products to create</h2>
          <br />
          <ul>
          ${entry.cart.length > 0
            && entry.cart.map((cartItem) => (
              `<li>Item: ${cartItem.Title}</li>
              <br/>
              <li>Quantity: ${cartItem.quantity}</li>
              <br />`
              ))}
          </ul>
          <br />
          <hr />
          <br />
          <h2>Customer Information:</h2>
          <br />
          <ul>
          <li>Name: ${entry.customerName}</li>
          <br />
          <li>Phone: ${entry.phoneNumber}</li>
          <br />
          <li>Email: ${entry.email}</li>
          <br />
          <li>Address: ${entry.addressStreet}, ${entry.addressCity} ${entry.addressState}</li>
          <br />
          </ul>
          <br />
          <hr />
          <br />
          ${entry.specialInstructions
            && `<h2>Special Instructions:</h2>
            <br />
            <p>${entry.specialInstructions}</p>
            <br />`
          }
        `
      });
    }

    return entry;
  }
};

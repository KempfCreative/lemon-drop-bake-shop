'use strict';

const { sanitizeEntity } = require('strapi-utils');
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async find(ctx) {
    let entities;
    if (ctx.query._q) {
      entities = await strapi.services.page.search(ctx.query);
    } else {
      entities = await strapi.services.page.find(ctx.query);
    }
    if (entities.length === 0) {
      ctx.notFound('No page found!');
    }
    if (entities.length === 1) {
      return sanitizeEntity(entities[0], { model: strapi.models.page })
    }
    return entities.map(entity =>
      sanitizeEntity(entity, { model: strapi.models.page })
    );
  },
};

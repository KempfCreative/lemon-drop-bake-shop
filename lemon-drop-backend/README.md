# Strapi application

A quick description of your Strapi application

## Docker

```
docker build \
--build-arg NODE_ENV=development \
--build-arg DATABASE_HOST=docker.for.mac.host.internal \
--build-arg DATABASE_NAME=lemon-drop-dev \
--build-arg DATABASE_USERNAME= \
--build-arg DATABASE_PASSWORD= \
-t lemon-drop-backend .
```

```
docker run -p 1337:1337 \
    -e "DATABASE_HOST=docker.for.mac.host.internal" \
    -e "DATABASE_NAME=lemon-drop-dev" \
    -e "DATABASE_USERNAME=" \
    -e "DATABASE_PASSWORD=" \
    lemon-drop-backend:latest
```
